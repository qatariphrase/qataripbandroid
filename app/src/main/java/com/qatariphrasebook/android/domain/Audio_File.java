package com.qatariphrasebook.android.domain;

public class Audio_File {


    public String getFavourite() {
        return favourite;
    }

    public void setFavourite(String favourite) {
        this.favourite = favourite;
    }

    String id;
    String eng_title;
    String french_title;
    String arabic_title;
    String hindi_title;
    String nepali_title;

    public String getHindi_title() {
        return hindi_title;
    }

    public String getNepali_title() {
        return nepali_title;
    }

    public String getSpanish_title() {
        return spanish_title;
    }

    public String getFilipino_title() {
        return filipino_title;
    }

    public String getUrdu_title() {
        return urdu_title;
    }

    String spanish_title;
    String filipino_title;
    String urdu_title;
    private String arabi_in_english;
    String chinese_title;

    public String getChinese_title() {
        return chinese_title;
    }

    public String getTurkish_title() {
        return turkish_title;
    }

    String turkish_title;
    String file_name;
    String favourite;
    private String category;
    String sub_category;


    public String getSub_category() {
        return sub_category;
    }

    public void setSub_category(String sub_category) {
        this.sub_category = sub_category;
    }

    public String getArabi_in_english() {
        return arabi_in_english;
    }

    public void setArabi_in_english(String arabi_in_english) {
        this.arabi_in_english = arabi_in_english;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFile_name() {
        return file_name;
    }
/*	public Audio_File(String id, String file_name, String eng_title,
			String arabic_title) {
		super();
		this.id = id;
		this.file_name = file_name;
		this.eng_title = eng_title;
		this.arabic_title = arabic_title;
	}*/

    public Audio_File(String id, String eng_title, String french_title,
                      String hindi_title,String nepali_title,String spanish_title,
                      String filipino_title,String urdu_title,String arabic_title, String arabi_in_english,
                      String chinese_title,String turkish_title,String file_name,
                      String subcategory, String category, String fav) {
        super();
        this.id = id;
        this.eng_title = eng_title;
        this.french_title = french_title;
        this.arabic_title = arabic_title;
        this.hindi_title = hindi_title;
        this.nepali_title = nepali_title;
        this.spanish_title = spanish_title;
        this.filipino_title = filipino_title;
        this.urdu_title = urdu_title;
        this.arabi_in_english = arabi_in_english;
        this.chinese_title = chinese_title;
        this.turkish_title = turkish_title;
        this.file_name = file_name;
        this.favourite = fav;
        this.sub_category = subcategory;
        this.category = category;

    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getEng_title() {
        return eng_title;
    }

    public String getCategory() {
        return category;
    }

    public String   getFrench_title() {
        return french_title;
    }

    public void setEng_title(String eng_title) {
        this.eng_title = eng_title;
    }

    public String getArabic_title() {
        return arabic_title;
    }

    public void setArabic_title(String arabic_title) {
        this.arabic_title = arabic_title;
    }

}
