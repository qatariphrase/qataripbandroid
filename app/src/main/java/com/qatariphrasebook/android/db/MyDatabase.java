package com.qatariphrasebook.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class MyDatabase extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "Phrasebook1.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_PHRASEBOOK = "Phrasebook_DB";

    public MyDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }



    public Cursor getSubCategory(String subcategory) {

        String query = "select * from Phrasebook_DB where subcategory ='"+subcategory+"' ORDER BY sub_category_order_id" ;
        Log.v("query", query);
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        return c;

//        SQLiteDatabase db = getReadableDatabase();
//        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
//
//        String[] sqlSelect = {"id", "english", "french", "arabic", "arabic-english",
//                "audio", "subcategory", "category", "favourite"};
//        String sqlTables = "Phrasebook_DB";
//
//        // Log.w("cate", subcategory);
//
//        qb.setTables(sqlTables);
//        Cursor c = qb.query(db, sqlSelect, "subcategory = ?",
//                new String[]{subcategory}, null, null, null);
//
//        return c;

    }

    public Cursor getCategory(String category) {

        String query = "select * from Phrasebook_DB where category ='"+category+"' ORDER BY sub_category_order_id";
        Log.v("query", query);
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        return c;
    }

//    public Cursor getFrenchSubCategory(String subcategory) {
//
//        SQLiteDatabase db = getReadableDatabase();
//        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
//
//        String[] sqlSelect = {"id", "english", "french", "arabic", "arabic_english",
//                "audio", "sub_category", "category", "favourite"};
//        String sqlTables = "Phrasebook_DB";
//
//        // Log.w("cate", subcategory);
//
//        qb.setTables(sqlTables);
//        Cursor c = qb.query(db, sqlSelect, "sub_category = ?",
//                new String[]{subcategory}, null, null, null);
//
//        return c;
//
//    }

    public void setFavourite(String id, String value) {

        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("favourite", value);

        db.update(TABLE_PHRASEBOOK, cv, "id = ?", new String[]{id});

    }

    public Cursor searchFile(String term, int lang) {
        String query;
        if (lang == 1) {
             query = "select * from Phrasebook_DB where [french] LIKE "
                    + DatabaseUtils.sqlEscapeString("%" + term + "%");

        } else if (lang == 2) {
            query = "select * from Phrasebook_DB where [hindi] LIKE "
                    + DatabaseUtils.sqlEscapeString("%" + term + "%");
        }
        else if (lang == 3) {
            query = "select * from Phrasebook_DB where [spanish] LIKE "
                    + DatabaseUtils.sqlEscapeString("%" + term + "%");
        }
        else if (lang == 4) {
            query = "select * from Phrasebook_DB where [filipino] LIKE "
                    + DatabaseUtils.sqlEscapeString("%" + term + "%");
        }
        else if (lang == 5) {
            query = "select * from Phrasebook_DB where [urdu] LIKE "
                    + DatabaseUtils.sqlEscapeString("%" + term + "%");
        }
        else if (lang == 6) {
            query = "select * from Phrasebook_DB where [nepali] LIKE "
                    + DatabaseUtils.sqlEscapeString("%" + term + "%");
        }
        else if (lang == 7) {
            query = "select * from Phrasebook_DB where [chinese] LIKE "
                    + DatabaseUtils.sqlEscapeString("%" + term + "%");
        }
        else if (lang == 8) {
            query = "select * from Phrasebook_DB where [turkish] LIKE "
                    + DatabaseUtils.sqlEscapeString("%" + term + "%");
        }

        else {
             query = "select * from Phrasebook_DB where [english] LIKE "
                    + DatabaseUtils.sqlEscapeString("%" + term + "%");


        }

        Log.v("query", query);
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        return c;
        // + "\"%"
        // + term + "%\"";


    }

    public Cursor getFavourites() {

        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.query(TABLE_PHRASEBOOK, null, "favourite = ?",
                new String[]{"true"}, null, null, null);

        return c;
    }

}