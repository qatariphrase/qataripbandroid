package com.qatariphrasebook.android.view.activity;

import com.qatariphrasebook.android.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

/**Splash Screen
 * @author Suhail
 *
 */
public class SplashActivity extends Activity {
	
	
	CountDownTimer timer;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		timer=new CountDownTimer(2000,1000) {
			
			@Override
			public void onTick(long millisUntilFinished) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				
				startActivity(new Intent(SplashActivity.this, ContainerActivity.class));
				finish();
			}
		};
		timer.start();
		
		
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		/*if(timer!=null)
			timer.cancel();*/
	}
@Override
public void onBackPressed() {
	// TODO Auto-generated method stub
}
}
