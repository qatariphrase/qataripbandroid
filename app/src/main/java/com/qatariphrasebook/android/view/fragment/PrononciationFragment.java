package com.qatariphrasebook.android.view.fragment;


import static com.qatariphrasebook.android.view.utils.Constants.LANG_CHINESE;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_ENGLISH;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_FILIPINO;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_FRENCH;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_HINDI;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_NEPALI;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_SPANISH;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_TURKISH;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_URDU;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.fragment.app.Fragment;


import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnPageScrollListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.qatariphrasebook.android.R;


public class PrononciationFragment extends Fragment implements OnPageChangeListener {

    WebView wv_prono;
    int pageNumber = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_pronounce, container,
                false);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        wv_prono = (WebView) getView().findViewById(R.id.wv_pronounce);
        PDFView pdfView = (PDFView) getView().findViewById(R.id.pdfv);

        SharedPreferences preferences = getActivity().getSharedPreferences("temp", getActivity().MODE_PRIVATE);
        int lang = preferences.getInt("language", 0);
        if (lang == LANG_ENGLISH) {
            wv_prono.setVisibility(View.VISIBLE);
            pdfView.setVisibility(View.GONE);
            wv_prono.loadDataWithBaseURL("file:///android_res/drawable/",
                    "<img src='prono.jpg' />", "text/html", "utf-8", null);


        } else {
            wv_prono.setVisibility(View.GONE);
            pdfView.setVisibility(View.VISIBLE);

            if (lang == LANG_FRENCH) loadpdf(pdfView,"french_guide.pdf");

            else if (lang == LANG_URDU) loadpdf(pdfView,"urdu_guide.pdf");
            else if (lang == LANG_SPANISH) loadpdf(pdfView,"spanish_guide.pdf");
            else if (lang == LANG_NEPALI) loadpdf(pdfView,"nepali_guide.pdf");
            else if (lang == LANG_FILIPINO) loadpdf(pdfView,"filipino_guide.pdf");
            else if (lang == LANG_HINDI) loadpdf(pdfView,"hindi_guide.pdf");
            else if (lang == LANG_CHINESE) loadpdf(pdfView,"chinese_guide.pdf");
            else if (lang == LANG_TURKISH) loadpdf(pdfView,"turkish_guide.pdf");


        }

        wv_prono.setVerticalScrollBarEnabled(true);
        wv_prono.getSettings().setBuiltInZoomControls(true);
        wv_prono.getSettings().setLoadWithOverviewMode(true);
        wv_prono.getSettings().setUseWideViewPort(true);


    }

    private void loadpdf(PDFView pdfView, String url) {
        pdfView.fromAsset(url)
                .defaultPage(pageNumber)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .scrollHandle(new DefaultScrollHandle(requireActivity()))
                .spacing(10)
                .pageFitPolicy(FitPolicy.WIDTH)
                .enableSwipe(true)// in dp
                .load();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Auto-generated method stub
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
    }
}
