package com.qatariphrasebook.android.view.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.qatariphrasebook.android.R;
import com.qatariphrasebook.android.view.activity.ContainerActivity;
import com.qatariphrasebook.android.view.activity.ContainerActivity_two;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class CategoryFragment extends Fragment implements OnClickListener {


    RelativeLayout ll1, ll2, ll3, ll4, ll5, ll6, ll7, ll8, ll9, ll10, ll11,
            ll12, ll13, ll14, ll15, ll16;

    private int lang;


	/*int[] array_int = new int[] {
			0,    15,
			5,    5,
			10,   15, 
			0,   5, 
			5,    0,
			10,    15,
			 5, 	0 ,
			
			0};
	

	int[] array_int1 = new int[] {
			0,    2,
			1,    1,
			2,   2, 
			0,   1, 
			2,    3,
			1,    1,
			 0, 	2 ,
			
			1};*/

    /**
     * height value for category tile
     */
    int[] array_int2 = new int[]{

            1, 2,
            2, 1,
            1, 2,
            2, 1,
            1, 2,
            2, 1,
            1, 2,
            1, 1


    };

    /**
     * Color code for categories
     */
    String[] array_colors = new String[]{

            "#007DBA", "#669044",
            "#8B807A", "#EFC24D",
            "#BAE0F5", "#681F79",


            "#97002D", "#DD743D",
            "#69C0B7", "#6BADDF",
            "#113819", "#004A8E",

            "#CDC6BE", "#EE4121",
            "#9B1A6C", "#007DBA"
    };

    /**
     * Names of category tiles
     */
//    String[] array_cat_names = new String[]{"Basics", "Numbers",
//            "Time & Dates", "Weather", "Money & Banks", "Transportation",
//            "Emergency", "Travel", "Food & Drinks", "At the Restaurants",
//            "Hotel", "Shopping", "Places & Attractions", "Health", "Pronunciation Guide"};

    /**
     * Category tiles images
     */
    int[] array_cat_img = new int[]{R.drawable.basics, R.drawable.ic_sports, R.drawable.numbers, R.drawable.time_dates,
            R.drawable.weather, R.drawable.money_banks, R.drawable.transportations, R.drawable.emergency,
            R.drawable.travel, R.drawable.food_drinks, R.drawable.at_the_resturants, R.drawable.hotel,
            R.drawable.shopping, R.drawable.places_attraction, R.drawable.health, R.drawable.pronunciation_guide};

    ArrayList<RelativeLayout> ll_array = new ArrayList<RelativeLayout>();
    private String[] array_cat_names;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.activity_category, container,
                false);
        ((ContainerActivity) getActivity()).enableDrawer(false);

        SharedPreferences preferences = getActivity().getSharedPreferences("temp", getActivity().MODE_PRIVATE);
        lang = preferences.getInt("language", 0);
        array_cat_names = new String[]{getString(R.string.basics), getString(R.string.sports), getString(R.string.Numbers),
                getString(R.string.timeDate), getString(R.string.weather), getString(R.string.moneyBank), getString(R.string.transportation),
                getString(R.string.emergency), getString(R.string.travel), getString(R.string.foodDrink), getString(R.string.at_the_restaurants),
                getString(R.string.hotel), getString(R.string.Shopping), getString(R.string.places_and_attractions), getString(R.string.health), getString(R.string.pronounciation_guide)};

        return view;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        initUi();
        registerUiEvents();
        DisplayMetrics display = new DisplayMetrics();

        getActivity().getWindowManager().getDefaultDisplay().getMetrics(display);

        int height = display.heightPixels;
        int width = display.widthPixels;

//		height = height - getActivity().getActionBar().getHeight();


        height = height / 3;
        width = width / 2;
        height = width;

        int h_jaba = height / 5;
        int size = ll_array.size();
        for (int i = 0; i < size; i++) {


            int h_new = height + (array_int2[i] * h_jaba);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    width - 6, h_new);
            params.setMargins(3, 3, 3, 3);
            if (i == size - 2) {

                h_new = height + (array_int2[i] * h_jaba) + 6;//-(20* 10);
                params = new LinearLayout.LayoutParams(
                        width - 6, h_new);
                params.setMargins(3, 3, 3, 0);
            }


            ll_array.get(i).setLayoutParams(params);


            ll_array.get(i).setBackgroundColor(Color.parseColor(array_colors[i]));

            LinearLayout rl_middle = (LinearLayout) ll_array.get(i).getChildAt(0);

            TextView tv = (TextView) rl_middle.getChildAt(1);
            tv.setText(array_cat_names[i]);
            ImageView iv = (ImageView) rl_middle.getChildAt(0);
            iv.setImageResource(array_cat_img[i]);

        }
    }


    private void initUi() {
        // TODO Auto-generated method stub

        ll1 = (RelativeLayout) getView().findViewById(R.id.category_1);

        ll2 = (RelativeLayout) getView().findViewById(R.id.category_2);
        ll3 = (RelativeLayout) getView().findViewById(R.id.category_3);
        ll4 = (RelativeLayout) getView().findViewById(R.id.category_4);
        ll5 = (RelativeLayout) getView().findViewById(R.id.category_5);
        ll6 = (RelativeLayout) getView().findViewById(R.id.category_6);

        ll7 = (RelativeLayout) getView().findViewById(R.id.category_7);
        ll8 = (RelativeLayout) getView().findViewById(R.id.category_8);
        ll9 = (RelativeLayout) getView().findViewById(R.id.category_9);
        ll10 = (RelativeLayout) getView().findViewById(R.id.category_10);
        ll11 = (RelativeLayout) getView().findViewById(R.id.category_11);
        ll12 = (RelativeLayout) getView().findViewById(R.id.category_12);

        ll13 = (RelativeLayout) getView().findViewById(R.id.category_13);
        ll14 = (RelativeLayout) getView().findViewById(R.id.category_14);
        ll15 = (RelativeLayout) getView().findViewById(R.id.category_15);
        ll16 = (RelativeLayout) getView().findViewById(R.id.category_16);

        ll_array.add(ll1);
        ll_array.add(ll16);
        ll_array.add(ll2);
        ll_array.add(ll3);
        ll_array.add(ll4);
        ll_array.add(ll5);
        ll_array.add(ll6);

        ll_array.add(ll7);
        ll_array.add(ll8);
        ll_array.add(ll9);
        ll_array.add(ll10);
        ll_array.add(ll11);
        ll_array.add(ll12);

        ll_array.add(ll13);
        ll_array.add(ll15);
        ll_array.add(ll14);


    }

    private void registerUiEvents() {
        // TODO Auto-generated method stub

        ll1.setOnClickListener(this);
        ll2.setOnClickListener(this);
        ll3.setOnClickListener(this);
        ll4.setOnClickListener(this);
        ll5.setOnClickListener(this);
        ll6.setOnClickListener(this);
        ll7.setOnClickListener(this);
        ll8.setOnClickListener(this);
        ll9.setOnClickListener(this);
        ll10.setOnClickListener(this);
        ll11.setOnClickListener(this);
        ll12.setOnClickListener(this);
        ll13.setOnClickListener(this);
        ll14.setOnClickListener(this);
        ll15.setOnClickListener(this);
        ll16.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        Intent i = new Intent(getActivity(), ContainerActivity_two.class);
        switch (v.getId()) {

            case R.id.category_1:


                i.putExtra("Title", getString(R.string.basics));
                i.putExtra("Title_fr", "Les Bases");
                i.putExtra("subcategory", "basics");
                i.putExtra("position", "0");
                break;

            case R.id.category_16:
                i.putExtra("Title", getString(R.string.sports));
                i.putExtra("Title_fr", "Santé");
                i.putExtra("subcategory", "sports");
                i.putExtra("position", "1");


                break;
            case R.id.category_2:

                i.putExtra("Title", getString(R.string.Numbers));
                i.putExtra("Title_fr", "Nombres");
                i.putExtra("subcategory", "numbers");
                i.putExtra("position", "2");


                break;

            case R.id.category_3:


                i.putExtra("Title", getString(R.string.timeDate));
                i.putExtra("Title_fr", "Heure et Date");
                i.putExtra("subcategory", "time_and_dates");
                i.putExtra("position", "3");


                break;

            case R.id.category_4:

			/*if(player!=null)
				if(player.isInitialized())
				player.pause();;*/

                i.putExtra("Title", getString(R.string.weather));
                i.putExtra("Title_fr", "La temps");
                i.putExtra("subcategory", "weather");
                i.putExtra("position", "4");


                break;

            case R.id.category_5:

                i.putExtra("Title", getString(R.string.moneyBank));
                i.putExtra("Title_fr", "Argent et banques");
                i.putExtra("subcategory", "money_and_banks");
                i.putExtra("position", "5");


                break;

            case R.id.category_6:
                i.putExtra("Title", getString(R.string.transportation));
                i.putExtra("Title_fr", "Les Transport");
                i.putExtra("subcategory", "transportations");
                i.putExtra("position", "6");


                break;

            case R.id.category_7:

                i.putExtra("Title", getString(R.string.emergency));
                i.putExtra("Title_fr", "Urgence");
                i.putExtra("subcategory", "emergency");
                i.putExtra("position", "7");

                break;

            case R.id.category_8:

                i.putExtra("Title", getString(R.string.travel));
                i.putExtra("Title_fr", "Voyage");
                i.putExtra("subcategory", "travel");
                i.putExtra("position", "8");

                break;

            case R.id.category_9:
                i.putExtra("Title", getString(R.string.foodDrink));
                i.putExtra("Title_fr", "Nourriture et boissons");
                i.putExtra("subcategory", "food_and_drinks");
                i.putExtra("position", "9");

                break;

            case R.id.category_10:
                i.putExtra("Title", getString(R.string.at_the_restaurants));
                i.putExtra("Title_fr", "Aux Restaurants");
                i.putExtra("subcategory", "at_the_restaurants");
                i.putExtra("position", "10");

                break;

            case R.id.category_11:
                i.putExtra("Title", getString(R.string.hotel));
                i.putExtra("Title_fr", "Cenregistrement À la hÔtel");
                i.putExtra("subcategory", "hotel");
                i.putExtra("position", "11");

                break;

            case R.id.category_12:
                i.putExtra("Title", getString(R.string.shopping));
                i.putExtra("Title_fr", "Achats");
                i.putExtra("subcategory", "shopping");
                i.putExtra("position", "12");
                break;

            case R.id.category_13:
                i.putExtra("Title", getString(R.string.places_and_attractions));
                i.putExtra("Title_fr", "Lieux et attractions");
                i.putExtra("subcategory", "places_and_attractions");
                i.putExtra("position", "13");

                break;

            case R.id.category_14:
                i.putExtra("Title", getString(R.string.pronounciation_guide));
                i.putExtra("Title_fr", "Guide de prononciation");
                i.putExtra("subcategory", "pronunciation");
                i.putExtra("position", "15");


                break;

            case R.id.category_15:
                i.putExtra("Title", getString(R.string.health));
                i.putExtra("Title_fr", "Santé");
                i.putExtra("subcategory", "health");
                i.putExtra("position", "14");

                break;


            default:
                break;
        }


        startActivity(i);
    }


    /**
     * Not Used
     *
     * @param filename
     */
//    public void playThisFile(String filename) {
//        if (player != null) {
//            if (player.isPaused() || player.isFinished()) {
//
//                if (player.isFinished()) {
//                    playSound(filename);
//
//                    Log.w("sound", "1");
//
//                } else {
//
//                    Log.w("sound", "2");
//                    player.start();
//                }
//
//			/*} else {
//				if(!player.isLooping())
//				{
//				playSound(filename);
//
//				Log.w("sound", "3");
//				}*/
//            }
//
//        } else {
//
//            Log.w("sound", "4");
//            playSound(filename);
//
//        }
//    }


    /**
     * Not Used
     *
     * @param filename
     * @return
     */
    private File play(String filename) {
        // File file = //...


        File file = null;
        try {
            File newFolder = new File(
                    Environment.getExternalStorageDirectory(), "TestFolder");
            if (!newFolder.exists()) {
                newFolder.mkdir();
            }
            try {
                file = new File(newFolder, filename.substring(0, filename.length() - 4) + ".ogg");
                file.createNewFile();
            } catch (Exception ex) {
                System.out.println("ex: " + ex);
            }
        } catch (Exception e) {
            System.out.println("e: " + e);
        }
        FileDescriptor fd = null;
        try {
            // fd = getAssets().openFd("3.ogg").getFileDescriptor();


            InputStream is = getActivity().getAssets().open(filename);
            long file_length = getActivity().getAssets().openFd(filename).getLength();
            if (getAvailableSpaceInBytes() > file_length) {
                OutputStream outputStream = new FileOutputStream(file);
                IOUtils.copy(is, outputStream);
                outputStream.close();
            } else {
                Toast.makeText(getActivity(), "No space in Sdcard. Free some space First", Toast.LENGTH_SHORT).show();
            }


        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }


        return file;
    }

    /**
     * Not Used
     *
     * @param filename
     */
//    public void playSound(String filename) {
//
//        File file = play(filename);
//        String path = file.getAbsolutePath();
//        try {
//
//            player = new SoundStreamAudioPlayer(1, path, 0.5f, 0.0f);
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//
//        new Thread(player).start();
//        player.start();
//
//        player.setOnProgressChangedListener(new OnProgressChangedListener() {
//
//            @Override
//            public void onTrackEnd(int track) {
//                // TODO Auto-generated method stub
//                player.stop();
//            }
//
//            @Override
//            public void onProgressChanged(int track, double currentPercentage,
//                                          long position) {
//                // TODO Auto-generated method stub
//
//            }
//
//            @Override
//            public void onExceptionThrown(String string) {
//                // TODO Auto-generated method stub
//
//            }
//        });
//    }


    /**
     * Not Used
     *
     * @return
     */
    public long getAvailableSpaceInBytes() {

        long availableSpace = -1L;
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        availableSpace = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();

        Toast.makeText(getActivity(), "" + availableSpace, Toast.LENGTH_SHORT).show();

        return availableSpace;
    }
}
