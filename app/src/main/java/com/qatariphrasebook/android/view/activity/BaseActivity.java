package com.qatariphrasebook.android.view.activity;

import static com.qatariphrasebook.android.view.utils.Constants.LANG_CHINESE;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_ENGLISH;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_FILIPINO;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_FRENCH;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_HINDI;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_NEPALI;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_SPANISH;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_TURKISH;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_URDU;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.qatariphrasebook.android.R;

import java.util.Locale;

public class BaseActivity extends AppCompatActivity {

    public PopupWindow lang_popup;
    private SharedPreferences.Editor editor;
    private SharedPreferences preferences;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);

    }

    public void showLanguagePopupList() {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.popup_language, null);
        if (lang_popup == null) {
            lang_popup = new PopupWindow(layout, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lang_popup.setAnimationStyle(android.R.style.Animation_Toast);
            lang_popup.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
            lang_popup.setOutsideTouchable(true);
        }

        TextView tv_english = (TextView) layout.findViewById(R.id.tv_english);
        tv_english.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lang_popup.dismiss();
                preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
                editor = preferences.edit();
                editor.putInt("language", LANG_ENGLISH);
                editor.commit();
                Intent i = new Intent(BaseActivity.this, ContainerActivity.class);

                startActivity(i);
                finish();

            }
        });
        TextView tv_french = (TextView) layout.findViewById(R.id.tv_french);
        tv_french.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lang_popup.dismiss();
                preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
                editor = preferences.edit();
                editor.putInt("language", LANG_FRENCH);
                editor.commit();
                Intent i = new Intent(BaseActivity.this, ContainerActivity.class);

                startActivity(i);
                finish();
            }
        });

        TextView tv_hindi =  layout.findViewById(R.id.tv_hindi);
        tv_hindi.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lang_popup.dismiss();
                preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
                editor = preferences.edit();
                editor.putInt("language", LANG_HINDI);
                editor.commit();
                startActivity(new Intent(BaseActivity.this, ContainerActivity.class));
                finish();
            }
        });
        TextView tv_spanish =  layout.findViewById(R.id.tv_spanish);
        tv_spanish.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lang_popup.dismiss();
                preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
                editor = preferences.edit();
                editor.putInt("language", LANG_SPANISH);
                editor.commit();
                startActivity(new Intent(BaseActivity.this, ContainerActivity.class));
                finish();
            }
        });

        TextView tv_filipino =  layout.findViewById(R.id.tv_filipino);
        tv_filipino.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lang_popup.dismiss();
                preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
                editor = preferences.edit();
                editor.putInt("language", LANG_FILIPINO);
                editor.commit();
                startActivity(new Intent(BaseActivity.this, ContainerActivity.class));
                finish();
            }
        });

TextView tv_urdu =  layout.findViewById(R.id.tv_urdu);
        tv_urdu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lang_popup.dismiss();
                preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
                editor = preferences.edit();
                editor.putInt("language", LANG_URDU);
                editor.commit();
                startActivity(new Intent(BaseActivity.this, ContainerActivity.class));
                finish();
            }
        });

TextView tv_nepali =  layout.findViewById(R.id.tv_nepali);
        tv_nepali.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lang_popup.dismiss();
                preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
                editor = preferences.edit();
                editor.putInt("language", LANG_NEPALI);
                editor.commit();
                startActivity(new Intent(BaseActivity.this, ContainerActivity.class));
                finish();
            }
        });

        TextView tv_chinese =  layout.findViewById(R.id.tv_chinese);
        tv_chinese.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lang_popup.dismiss();
                preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
                editor = preferences.edit();
                editor.putInt("language", LANG_CHINESE);
                editor.commit();
                startActivity(new Intent(BaseActivity.this, ContainerActivity.class));
                finish();
            }
        });

        TextView tv_turkish =  layout.findViewById(R.id.tv_turkish);
        tv_turkish.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lang_popup.dismiss();
                preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
                editor = preferences.edit();
                editor.putInt("language", LANG_TURKISH);
                editor.commit();
                startActivity(new Intent(BaseActivity.this, ContainerActivity.class));
                finish();
            }
        });




        lang_popup.setOutsideTouchable(true);
        if (lang_popup.isShowing())
            lang_popup.dismiss();
        else
            lang_popup.showAsDropDown(findViewById(R.id.ll_language), 0, 30);

    }

    public void setLanguage(String localeValue){
        Locale locale = new Locale(localeValue);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        conf.setLayoutDirection(locale);
        res.updateConfiguration(conf, dm);
    }
}
