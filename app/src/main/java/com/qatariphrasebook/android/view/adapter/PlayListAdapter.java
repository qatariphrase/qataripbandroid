package com.qatariphrasebook.android.view.adapter;

import static com.qatariphrasebook.android.view.utils.Constants.LANG_CHINESE;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_FILIPINO;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_FRENCH;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_HINDI;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_NEPALI;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_SPANISH;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_TURKISH;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_URDU;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qatariphrasebook.android.R;
import com.qatariphrasebook.android.domain.Audio_File;
import com.qatariphrasebook.android.view.activity.ContainerActivity_two;

import java.util.ArrayList;

public class PlayListAdapter extends BaseAdapter {

    private int lang;
    ArrayList<Audio_File> arrayList;
    Context context;
    LayoutInflater inflater;
    int selected_position;
    String identifier = "play,0";
    PlayAudio playAudio;

    public PlayListAdapter(ArrayList<Audio_File> arrayList, Context context, int lang) {
        super();
        this.arrayList = arrayList;
        this.context = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        selected_position = -1;
        this.lang = lang;
//        this.playAudio = playAudio;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_playlist, null);
            holder = new ViewHolder();
            holder.rl_container = (RelativeLayout) convertView.findViewById(R.id.rl_container_row);
            holder.iv_play = (ImageView) convertView.findViewById(R.id.iv_play);
            holder.tv_arabic = (TextView) convertView.findViewById(R.id.tv_arabic);
            holder.tv_english = (TextView) convertView.findViewById(R.id.tv_english);
            holder.tv_arabic_in_english = (TextView) convertView.findViewById(R.id.tv_arabi_in_english);
            holder.iv_fav = (ImageView) convertView.findViewById(R.id.iv_fav);
            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();
        }

String title;
        if (lang == LANG_FRENCH) {
            title = arrayList.get(position).getFrench_title();

        } else if(lang == LANG_HINDI)
            title = arrayList.get(position).getHindi_title();
        else if(lang == LANG_SPANISH)
            title = arrayList.get(position).getSpanish_title();
        else if(lang == LANG_NEPALI)
            title = arrayList.get(position).getNepali_title();
        else if(lang == LANG_FILIPINO)
            title = arrayList.get(position).getFilipino_title();
        else if(lang == LANG_URDU)
            title = arrayList.get(position).getUrdu_title();
        else if(lang == LANG_CHINESE)
            title = arrayList.get(position).getChinese_title();
        else if(lang == LANG_TURKISH)
            title = arrayList.get(position).getTurkish_title();

        else {
            title = arrayList.get(position).getEng_title();
        }

        holder.tv_english.setText(title);
        holder.tv_arabic.setText(arrayList.get(position).getArabic_title());
        holder.tv_arabic_in_english.setText(arrayList.get(position).getArabi_in_english());


        if (selected_position == position) {
//            holder.rl_container.setBackgroundColor(Color.parseColor("#203153"));
            holder.rl_container.setBackgroundResource(R.drawable.round_bg);
            holder.iv_fav.setImageResource(R.drawable.star_blank_white);


            holder.tv_english.setTextColor(Color.parseColor("#ffffff"));
            holder.tv_arabic.setTextColor(Color.parseColor("#ffffff"));
            holder.tv_arabic_in_english.setTextColor(Color.parseColor("#ffffff"));
            holder.iv_play.setImageResource(R.drawable.pause);

            if (identifier.equalsIgnoreCase("play")) {
                holder.iv_play.setImageResource(R.drawable.play);
            } else {
                holder.iv_play.setImageResource(R.drawable.pause);
            }


        } else {
            holder.rl_container.setBackgroundResource(R.drawable.round_bg_white);
//            holder.rl_container.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.tv_english.setTextColor(Color.parseColor("#000000"));
            holder.tv_arabic.setTextColor(Color.parseColor("#000000"));
            holder.tv_arabic_in_english.setTextColor(Color.parseColor("#000000"));
            holder.iv_play.setImageResource(R.drawable.play);

        }

//        if (selected_position < 0) {
//            holder.rl_container.setBackgroundResource(R.drawable.round_bg_white);
//            if (position == 0) {
////                holder.rl_container.setBackgroundColor(Color.parseColor("#203153"));
//                holder.rl_container.setBackgroundResource(R.drawable.round_bg);
//                holder.tv_english.setTextColor(Color.parseColor("#ffffff"));
//                holder.tv_arabic.setTextColor(Color.parseColor("#ffffff"));
//                holder.tv_arabic_in_english.setTextColor(Color.parseColor("#ffffff"));
//            }
//        }

//        holder.rl_container.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                selected_position = position;
//                identifier = "pause";
//                notifyDataSetChanged();
//                playAudio.onPlayAudio(position);
//            }
//        });

        if (arrayList.get(position).getFavourite().equalsIgnoreCase("false")) {

            if (selected_position == position)
                holder.iv_fav.setImageResource(R.drawable.star_blank_white);
            else
                holder.iv_fav.setImageResource(R.drawable.star_blank);
        } else {
            holder.iv_fav.setImageResource(R.drawable.star_fav);
        }


        holder.iv_fav.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (arrayList.get(position).getFavourite().equalsIgnoreCase("false")) {
                    holder.iv_fav.setImageResource(R.drawable.star_fav);
                    ((ContainerActivity_two) context).getDB().setFavourite(arrayList.get(position).getId(), "true");
                    arrayList.get(position).setFavourite("true");
                } else {
                    holder.iv_fav.setImageResource(R.drawable.star_blank);
                    ((ContainerActivity_two) context).getDB().setFavourite(arrayList.get(position).getId(), "false");
                    arrayList.get(position).setFavourite("false");
//					arrayList.remove(position);

                }

                notifyDataSetChanged();

            }
        });
		
		
		
		/*
		holder.rl_container.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
			
				
			}
		});*/

        return convertView;
    }


    public void setPlayState(int position, String tag) {
        selected_position = position;
        identifier = tag;
        notifyDataSetChanged();
    }

    private static class ViewHolder {


        ImageView iv_play, iv_fav;
        TextView tv_english, tv_arabic, tv_arabic_in_english;
        RelativeLayout rl_container;

    }

    public interface PlayAudio {
        public void onPlayAudio(int position);
    }
}
