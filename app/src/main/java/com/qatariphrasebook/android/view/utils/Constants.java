package com.qatariphrasebook.android.view.utils;

public class Constants {

    public static int LANG_ENGLISH = 0;
    public static int LANG_FRENCH = 1;
    public static int LANG_HINDI = 2;
    public static int LANG_SPANISH = 3;
    public static int LANG_FILIPINO = 4;
    public static int LANG_URDU = 5;
    public static int LANG_NEPALI = 6;
    public static int LANG_CHINESE = 7;
    public static int LANG_TURKISH = 8;
}
