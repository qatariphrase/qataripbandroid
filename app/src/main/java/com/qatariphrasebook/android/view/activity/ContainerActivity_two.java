package com.qatariphrasebook.android.view.activity;

import static com.qatariphrasebook.android.view.utils.Constants.LANG_URDU;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.qatariphrasebook.android.R;
import com.qatariphrasebook.android.db.MyDatabase;
import com.qatariphrasebook.android.view.dialog.AboutUs_Dialog;
import com.qatariphrasebook.android.view.dialog.SpeedControl_Dialog;
import com.qatariphrasebook.android.view.fragment.Favouritefragment;
import com.qatariphrasebook.android.view.fragment.Playlistfragment;
import com.qatariphrasebook.android.view.fragment.PrononciationFragment;
import com.qatariphrasebook.android.view.fragment.Searchlistfragment;
import com.qatariphrasebook.android.view.fragment.SideMenuFragment;

import java.util.Locale;

public class ContainerActivity_two extends BaseActivity {

    private DrawerLayout mDrawerLayout;
    // private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private CharSequence Title_fr;
    private String position;
    private String db_subcategory;
    private FrameLayout fl_sidemenu, fl_content;
    public Toolbar toolbar;
    private MenuView mv_search;
    private float prev_offset, prev_offset_scale = 1;

    public PopupWindow popup;

    public MyDatabase DB;

    private boolean is_float;
    private TextView tv_title;
    private ImageView home;
    private ImageView settings;
    private LinearLayout ll_language;
    private TextView tv_language;
    private SharedPreferences preferences;

    private int lang;
    private ImageView favourites;
    private EditText edt_search;
    private LinearLayout ll_search;
    private ImageView close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_container);
        ImageView logo = (ImageView) findViewById(R.id.logo);
        ImageView hamburger = (ImageView) findViewById(R.id.img_hamburger);
        logo.setVisibility(View.GONE);

        hamburger.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });


        DB = new MyDatabase(ContainerActivity_two.this);
        DB.getReadableDatabase();

        try {

            mTitle = getIntent().getStringExtra("Title");
            Title_fr = getIntent().getStringExtra("Title_fr");
            db_subcategory = getIntent().getStringExtra("subcategory");
            position = getIntent().getStringExtra("position");

            Log.e("fathima", "select item position " + position);


        } catch (Exception e) {

        }
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));
        // mDrawerList = (ListView) findViewById(R.id.left_drawer);
        fl_sidemenu = (FrameLayout) findViewById(R.id.left_drawer);
        fl_content = (FrameLayout) findViewById(R.id.content_frame);
        mv_search = (MenuView) findViewById(R.id.action_websearch);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        home = (ImageView) findViewById(R.id.home);
        favourites = (ImageView) findViewById(R.id.favourites);
        settings = (ImageView) findViewById(R.id.search);
        ll_language = (LinearLayout) findViewById(R.id.ll_language);
        tv_language = (TextView) findViewById(R.id.tv_language);
        tv_title = (TextView) findViewById(R.id.tv_title);
        edt_search = (EditText) findViewById(R.id.edt_search);
        ll_search = (LinearLayout) findViewById(R.id.ll_search);
        close = (ImageView) findViewById(R.id.close);
        favourites.setVisibility(View.GONE);
        home.setVisibility(View.VISIBLE);


        setLanguage();

        home.setImageResource(R.drawable.ic_home_white_24dp);
        settings.setImageResource(R.drawable.ic_settings_white_24dp);

        tv_title.setText(mTitle);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ContainerActivity_two.this, ContainerActivity.class));
                finish();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edt_search.setText("");
            }
        });

        favourites.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(ContainerActivity_two.this, ContainerActivity_two.class);
                i2.putExtra("Title", getString(R.string.Favourites));
                i2.putExtra("subcategory", "favourites");
                i2.putExtra("position", "0");
                startActivity(i2);
//                tv_title.setVisibility(View.VISIBLE);
//                edt_search.setVisibility(View.GONE);
//                selectItem(2);
            }
        });

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupList();
            }
        });

        ll_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLanguagePopupList();
            }
        });


        // set a custom shadow that overlays the main content when the drawer
        // opens
        mDrawerLayout.setDrawerShadow(R.color.transparent, GravityCompat.START);
        // mDrawerList.setAdapter(new ArrayAdapter<String>(this,
        // R.layout.drawer_list_item, array_cat_names));

        setsidemenu();
        // mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        setSupportActionBar(toolbar);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#203153")));

        if (db_subcategory.equalsIgnoreCase("search")) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            hamburger.setVisibility(View.GONE);
            ll_search.setVisibility(View.VISIBLE);
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(true);
//            getSupportActionBar().setIcon(R.drawable.home_btn);
        }

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
                mDrawerLayout, /* DrawerLayout object */
                null, /*
         * nav drawer image to replace 'Up' caret
         */
                R.string.app_name, /*
         * "open drawer" description for
         * accessibility
         */
                R.string.app_name /*
         * "close drawer" description for
         * accessibility
         */
        ) {
            public void onDrawerClosed(View view) {
//                if (lang == 1) {
//                    tv_title.setText(Title_fr);
//
//                } else {
                tv_title.setText(mTitle);
//                setTitles();

//                }
//                setLanguage();
                invalidateOptionsMenu();
                hideSoftKeyboard(ContainerActivity_two.this);
            }

            public void onDrawerOpened(View drawerView) {
                tv_title.setText(R.string.Category);

                invalidateOptionsMenu();

                if (popup != null)
                    popup.dismiss();

                hideSoftKeyboard(ContainerActivity_two.this);
                // showZoomOut(fl_content);
                // creates call to onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                // TODO Auto-generated method stub
                super.onDrawerSlide(drawerView, slideOffset);
                // Toast.makeText(ContainerActivity_two.this, ""+slideOffset,
                // Toast.LENGTH_SHORT).show();+

                AnimateContent(fl_content, slideOffset);

                String alpha = getAlphaValue(1 - slideOffset);

                /*
                 * if(alpha.length()>3) alpha=alpha.substring(2,4); else
                 * alpha=alpha.substring(2,3)+"0";
                 *
                 *
                 * if(slideOffset==1f) alpha="00"; if(slideOffset==0f)
                 * alpha="ff"; Log.w("alphau",""+slideOffset);
                 * Log.w("alpha",alpha);
                 */
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#" + alpha + "203153")));
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            Log.e("fathima", "select item subcategory is " + db_subcategory);
            if (db_subcategory.equalsIgnoreCase("search")) {
                // mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

                // getSupportActionBar().setIcon(R.drawable.back);
                selectItem(1);
            } else if (db_subcategory.equalsIgnoreCase("favourites")) {

                // mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

                // getSupportActionBar().setIcon(R.drawable.back);
                selectItem(2);
            } else if (db_subcategory.equalsIgnoreCase("pronunciation")) {

                selectItem(3);
            } else {
                // mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                selectItem(0);

            }
        }

        // Fragment f=new PlayList_Fragment();

        // getFragmentManager().beginTransaction().replace(R.id.fl_container,
        // f).commit();

        // initUi();

        // registerUiEvents();
        /*
         *
         * LayoutInflater mInflater = LayoutInflater.from(this);
         *
         * /*View mCustomView = mInflater.inflate(R.layout.actionbar, null);
         * android.app.ActionBar mActionBar = getSupportActionBar();
         * mActionBar.setDisplayShowHomeEnabled(false);
         * mActionBar.setDisplayShowTitleEnabled(false);
         * mActionBar.setCustomView(mCustomView);
         * mActionBar.setDisplayShowCustomEnabled(true);
         *
         * Button btn_dropdown = (Button) mCustomView
         * .findViewById(R.id.btn_dropdown); btn_dropdown.setOnClickListener(new
         * OnClickListener() {
         *
         * @Override public void onClick(View v) { // TODO Auto-generated method
         * stub
         *
         * showPopupList(v); } });
         */

    }

    private void setTitles() {
        tv_title.setText(getString(R.string.basics));
        if (lang == 1) {
           /* if (tv_title.getText().equals("Basics")) {
                tv_title.setText(getString(R.string.basics));
            }
            else */
            if (tv_title.getText().equals("Greetings")) {
                tv_title.setText("Salutations");
            } else if (tv_title.getText().equals("Expressions")) {
                tv_title.setText("Expressions");
            } else if (tv_title.getText().equals("Questions")) {
                tv_title.setText("Des questions");
            } else if (tv_title.getText().equals("Telephone")) {
                tv_title.setText("Téléphone");
            } else if (tv_title.getText().equals("Feelings")) {
                tv_title.setText("Sentiments");
            } else if (tv_title.getText().equals("Verbs")) {
                tv_title.setText("Verbs");

            } else if (tv_title.getText().equals("Numbers")) {
                tv_title.setText("Nombres");
            } else if (tv_title.getText().equals("Ordinate Numbers")) {
                tv_title.setText("Nombres ordonnés");
            } else if (tv_title.getText().equals("Amounts")) {
                tv_title.setText("Les montants");
            } else if (tv_title.getText().equals("Time & Dates")) {
                tv_title.setText("Heure et Date");
            } else if (tv_title.getText().equals("Telling the Time")) {
                tv_title.setText("Dire l'heure");
            } else if (tv_title.getText().equals("Days of the week")) {
                tv_title.setText("Jours de la semaine");
            } else if (tv_title.getText().equals("Western Calendar")) {
                tv_title.setText("Calendrier occidental");
            } else if (tv_title.getText().equals("Islamic Calendar")) {
                tv_title.setText("Calendrier islamique");
            } else if (tv_title.getText().equals("Weather")) {
                tv_title.setText("La temps");
            } else if (tv_title.getText().equals("Money & Banks")) {
                tv_title.setText("Argent et banques");
            } else if (tv_title.getText().equals("Banks Money")) {
                tv_title.setText("Argent et banques");
            } else if (tv_title.getText().equals("Bills and Cheques")) {
                tv_title.setText("Factures et chèques");
            } else if (tv_title.getText().equals("Transportation")) {
                tv_title.setText("Les Transport");
            } else if (tv_title.getText().equals("Directions")) {
                tv_title.setText("Directions");
            } else if (tv_title.getText().equals("Bus, Metro, Train")) {
                tv_title.setText("Bus, métro et train");
            } else if (tv_title.getText().equals("Taxi")) {
                tv_title.setText("Taxi");
            } else if (tv_title.getText().equals("Rent a car")) {
                tv_title.setText("Louer une voiture");
            } else if (tv_title.getText().equals("Emergency")) {
                tv_title.setText("Urgence");
            } else if (tv_title.getText().equals("On the Road")) {
                tv_title.setText("On the Road");
            } else if (tv_title.getText().equals("Problems")) {
                tv_title.setText("Problèmes");
            } else if (tv_title.getText().equals("Authorities")) {
                tv_title.setText("Les autorités");
            } else if (tv_title.getText().equals("Travel")) {
                tv_title.setText("Voyage");
            } else if (tv_title.getText().equals("At the Airport")) {
                tv_title.setText("À l'aéroport");
            } else if (tv_title.getText().equals("Passport, Customs")) {
                tv_title.setText("Passeport et douanes");
            } else if (tv_title.getText().equals("Food & Drinks")) {
                tv_title.setText("Nourriture et boissons");
            } else if (tv_title.getText().equals("Food, Groceries")) {
                tv_title.setText("la nourriture et l'épicerie");
            } else if (tv_title.getText().equals("Drinks")) {
                tv_title.setText("la boisson");
            } else if (tv_title.getText().equals("At the Restaurants")) {
                tv_title.setText("Aux Restaurants");


            } else if (tv_title.getText().equals("Hotel")) {
                tv_title.setText("Hôtel");
            } else if (tv_title.getText().equals("Checking In")) {
                tv_title.setText("Cenregistrement À la hÔtel");
            } else if (tv_title.getText().equals("Checking Out")) {
                tv_title.setText("Départ");
            } else if (tv_title.getText().equals("Shopping")) {
                tv_title.setText("Achats");
            } else if (tv_title.getText().equals("Colors")) {
                tv_title.setText("Couleurs");
            } else if (tv_title.getText().equals("Clothing")) {
                tv_title.setText("Vêtements");
            } else if (tv_title.getText().equals("Stationary")) {
                tv_title.setText("Stationnaire");


            } else if (tv_title.getText().equals("Places & Attractions")) {
                tv_title.setText("Lieux et attractions");
            } else if (tv_title.getText().equals("Sightseeing")) {
                tv_title.setText("Sightseeing");
            } else if (tv_title.getText().equals("Parts of the Body")) {
                tv_title.setText("Parts of the Body");
            } else if (tv_title.getText().equals("Doctors, Pharmacy")) {
                tv_title.setText("Doctors, Pharmacy");
            } else if (tv_title.getText().equals("Pronunciation Guide")) {
                tv_title.setText("Guide de prononciation");
            }
        }
    }

    private void setLanguage() {
        SharedPreferences preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
        lang = preferences.getInt("language", 0);
        if (lang == 1) {

            setLanguage("fr");
            tv_language.setText("FR");

//            if (mTitle.equals("Basics")) {
//                Title_fr = "Les bases";
//            } else if (mTitle.equals("Greetings")) {
//                Title_fr = "Salutations";
//            } else if (mTitle.equals("Expressions")) {
//                Title_fr = "Expressions";
//            } else if (mTitle.equals("Questions")) {
//                Title_fr = "Des questions";
//            } else if (mTitle.equals("Telephone")) {
//                Title_fr = "Téléphone";
//            } else if (mTitle.equals("Feelings")) {
//                Title_fr = "Sentiments";
//            } else if (mTitle.equals("Verbs")) {
//                Title_fr = "Verbs";
//
//            } else if (mTitle.equals("Numbers")) {
//                Title_fr = "Nombres";
//            } else if (mTitle.equals("Ordinate Numbers")) {
//                Title_fr = "Nombres ordonnés";
//            } else if (mTitle.equals("Amounts")) {
//                Title_fr = "Les montants";
//            } else if (mTitle.equals("Time & Dates")) {
//                Title_fr = "Heure et Date";
//            } else if (mTitle.equals("Telling the Time")) {
//                Title_fr = "Dire l'heure";
//            } else if (mTitle.equals("Days of the week")) {
//                Title_fr = "Jours de la semaine";
//            } else if (mTitle.equals("Western Calendar")) {
//                Title_fr = "Calendrier occidental";
//            } else if (mTitle.equals("Islamic Calendar")) {
//                Title_fr = "Calendrier islamique";
//            } else if (mTitle.equals("Weather")) {
//                Title_fr = "La temps";
//            } else if (mTitle.equals("Money & Banks")) {
//                Title_fr = "Argent et banques";
//            } else if (mTitle.equals("Banks Money")) {
//                Title_fr = "Argent et banques";
//            } else if (mTitle.equals("Bills and Cheques")) {
//                Title_fr = "Factures et chèques";
//            } else if (mTitle.equals("Transportation")) {
//                Title_fr = "Les Transport";
//            } else if (mTitle.equals("Directions")) {
//                Title_fr = "Directions";
//            } else if (mTitle.equals("Bus, Metro, Train")) {
//                Title_fr = "Bus, métro et train";
//            } else if (mTitle.equals("Taxi")) {
//                Title_fr = "Taxi";
//            } else if (mTitle.equals("Rent a car")) {
//                Title_fr = "Louer une voiture";
//            } else if (mTitle.equals("Emergency")) {
//                Title_fr = "Urgence";
//            } else if (mTitle.equals("On the Road")) {
//                Title_fr = "On the Road";
//            } else if (mTitle.equals("Problems")) {
//                Title_fr = "Problèmes";
//            } else if (mTitle.equals("Authorities")) {
//                Title_fr = "Les autorités";
//            } else if (mTitle.equals("Travel")) {
//                Title_fr = "Voyage";
//            } else if (mTitle.equals("At the Airport")) {
//                Title_fr = "À l'aéroport";
//            } else if (mTitle.equals("Passport, Customs")) {
//                Title_fr = "Passeport et douanes";
//            } else if (mTitle.equals("Food & Drinks")) {
//                Title_fr = "Nourriture et boissons";
//            } else if (mTitle.equals("Food, Groceries")) {
//                Title_fr = "la nourriture et l'épicerie";
//            } else if (mTitle.equals("Drinks")) {
//                Title_fr = "la boisson";
//            } else if (mTitle.equals("At the Restaurants")) {
//                Title_fr = "Aux Restaurants";
//
//
//            } else if (mTitle.equals("Hotel")) {
//                Title_fr = "Hôtel";
//            } else if (mTitle.equals("Checking In")) {
//                Title_fr = "Cenregistrement À la hÔtel";
//            } else if (mTitle.equals("Checking Out")) {
//                Title_fr = "Départ";
//            } else if (mTitle.equals("Shopping")) {
//                Title_fr = "Achats";
//            } else if (mTitle.equals("Colors")) {
//                Title_fr = "Couleurs";
//            } else if (mTitle.equals("Clothing")) {
//                Title_fr = "Vêtements";
//            } else if (mTitle.equals("Stationary")) {
//                Title_fr = "Stationnaire";
//
//
//            } else if (mTitle.equals("Places & Attractions")) {
//                Title_fr = "Lieux et attractions";
//            } else if (mTitle.equals("Sightseeing")) {
//                Title_fr = "Sightseeing";
//            } else if (mTitle.equals("Parts of the Body")) {
//                Title_fr = "Parts of the Body";
//            } else if (mTitle.equals("Doctors, Pharmacy")) {
//                Title_fr = "Doctors, Pharmacy";
//            } else if (mTitle.equals("Pronunciation Guide")) {
//                Title_fr = "Guide de prononciation";
//            }
//
//
//            tv_title.setText(Title_fr);


        } else if (lang == 2) {
            setLanguage("hi");
            tv_language.setText("HI");
        } else if (lang == 3) {
            setLanguage("es");
            tv_language.setText("ES");
        } else if (lang == 4) {
            setLanguage("fil");
            tv_language.setText("FIL");
        } else if (lang == 5) {
            setLanguage("ur");
            tv_language.setText("UR");
        }
        else if (lang == 6) {
            setLanguage("ne");
            tv_language.setText("NP");
        }
        else if (lang == 7) {
            setLanguage("zh");
            tv_language.setText("CN");
        }
        else if (lang == 8) {
            setLanguage("tr");
            tv_language.setText("TR");
        }
        else {

            setLanguage("en");
            tv_language.setText("EN");

//            if (mTitle.equals("Les bases")) {
//                mTitle = "Basics";
//            } else if (mTitle.equals("Salutations")) {
//                mTitle = "Greetings";
//            } else if (mTitle.equals("Expressions")) {
//                mTitle = "Expressions";
//            } else if (mTitle.equals("Des questions")) {
//                mTitle = "Questions";
//            } else if (mTitle.equals("Téléphone")) {
//                mTitle = "Telephone";
//            } else if (mTitle.equals("Sentiments")) {
//                mTitle = "Feelings";
//            } else if (mTitle.equals("Verbs")) {
//                mTitle = "Verbs";
//
//            } else if (mTitle.equals("Nombres")) {
//                mTitle = "Numbers";
//            } else if (mTitle.equals("Nombres ordonnés")) {
//                mTitle = "Ordinate Numbers";
//            } else if (mTitle.equals("Les montants")) {
//                mTitle = "Amounts";
//            } else if (mTitle.equals("Heure et Date")) {
//                mTitle = "Time & Dates";
//            } else if (mTitle.equals("Dire l'heure")) {
//                mTitle = "Telling the Time";
//            } else if (mTitle.equals("Jours de la semaine")) {
//                mTitle = "Days of the week";
//            } else if (mTitle.equals("Calendrier occidental")) {
//                mTitle = "Western Calendar";
//            } else if (mTitle.equals("Calendrier islamique")) {
//                mTitle = "Islamic Calendar";
//            } else if (mTitle.equals("La temps")) {
//                mTitle = "Weather";
//            } else if (mTitle.equals("Argent et banques")) {
//                mTitle = "Banks & Money";
//            } else if (mTitle.equals("Factures et chèques")) {
//                mTitle = "Bills and Cheques";
//            } else if (mTitle.equals("Les Transport")) {
//                mTitle = "Transportation";
//            } else if (mTitle.equals("Directions")) {
//                mTitle = "Directions";
//            } else if (mTitle.equals("Bus, métro et train")) {
//                mTitle = "Bus, Metro, Train";
//            } else if (mTitle.equals("Taxi")) {
//                mTitle = "Taxi";
//            } else if (mTitle.equals("Louer une voiture")) {
//                mTitle = "Rent a car";
//            } else if (mTitle.equals("Urgence")) {
//                mTitle = "Emergency";
//            } else if (mTitle.equals("On the Road")) {
//                mTitle = "On the Road";
//            } else if (mTitle.equals("Problèmes")) {
//                mTitle = "Problems";
//            } else if (mTitle.equals("Les autorités")) {
//                mTitle = "Authorities";
//            } else if (mTitle.equals("Voyage")) {
//                mTitle = "Travel";
//            } else if (mTitle.equals("À l'aéroport")) {
//                mTitle = "At the Airport";
//            } else if (mTitle.equals("Passeport et douanes")) {
//                mTitle = "Passport, Customs";
//            } else if (mTitle.equals("Nourriture et boissons")) {
//                mTitle = "Food & Drinks";
//            } else if (mTitle.equals("la nourriture et l'épicerie")) {
//                mTitle = "Food, Groceries";
//            } else if (mTitle.equals("la boisson")) {
//                mTitle = "Drinks";
//            } else if (mTitle.equals("Aux Restaurants")) {
//                mTitle = "At the Restaurants";
//
//
//            } else if (mTitle.equals("Hôtel")) {
//                mTitle = "Hotel";
//            } else if (mTitle.equals("Cenregistrement À la hÔtel")) {
//                mTitle = "Checking In";
//            } else if (mTitle.equals("Départ")) {
//                mTitle = "Checking Out";
//            } else if (mTitle.equals("Achats")) {
//                mTitle = "Shopping";
//            } else if (mTitle.equals("Couleurs")) {
//                mTitle = "Colors";
//            } else if (mTitle.equals("Vêtements")) {
//                mTitle = "Clothing";
//            } else if (mTitle.equals("Stationnaire")) {
//                mTitle = "Stationary";
//
//
//            } else if (mTitle.equals("Lieux et attractions")) {
//                mTitle = "Places & Attractions";
//            } else if (mTitle.equals("Sightseeing")) {
//                mTitle = "Sightseeing";
//            } else if (mTitle.equals("Parts of the Body")) {
//                mTitle = "Parts of the Body";
//            } else if (mTitle.equals("Doctors, Pharmacy")) {
//                mTitle = "Doctors, Pharmacy";
//            } else if (mTitle.equals("Guide de prononciation")) {
//                mTitle = "Pronunciation Guide";
//            }
//
//            tv_title.setText(mTitle);

        }


    }

    public MyDatabase getDB() {
        if (DB == null)
            DB = new MyDatabase(ContainerActivity_two.this);

        return DB;
    }

    /**
     * To get the alpha value based on how much the side menu is open
     *
     * @param i
     * @return
     */
    private String getAlphaValue(float i) {

        i = Math.round(i * 100) / 100.0f;
        int alpha = (int) Math.round(i * 255);
        String hex = Integer.toHexString(alpha).toUpperCase();
        if (hex.length() == 1)
            hex = "0" + hex;
        int percent = (int) (i * 100);
        System.out.println(String.format("%d%% � %s", percent, hex));
        return hex;

    }

    /**
     * Not used
     *
     * @param v
     * @param anim
     */
    public void showHistory(View v, String anim) {

        AnimatorSet set = new AnimatorSet();
        // Using property animation
        ObjectAnimator animation = ObjectAnimator.ofFloat(v, anim, 1f, 200f);

        animation.setDuration(2000);
        set.play(animation);
        set.start();

    }

    /**
     * Not Used
     *
     * @param v
     * @param anim
     */
    public void showHistoryY(View v, String anim) {

        AnimatorSet set = new AnimatorSet();
        // Using property animation
        ObjectAnimator animation = ObjectAnimator.ofFloat(v, anim, 200f, 1f);

        animation.setDuration(2000);
        set.play(animation);
        set.start();

    }

    /**
     * Not Used
     *
     * @param v
     */
    public void showZoomOut(View v) {

        AnimatorSet set = new AnimatorSet();
        // Using property animation
        ObjectAnimator animation1 = ObjectAnimator.ofFloat(v, "ScaleX", 1f, 0.6f);
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(v, "ScaleY", 1f, 0.6f);
        ObjectAnimator animation3 = ObjectAnimator.ofFloat(v, "translationX", 1f, 200f);

        set.setDuration(100);
        set.playTogether(animation1, animation2, animation3);
        set.start();

    }

    /**
     * Slides the content fragment
     *
     * @param v
     * @param offset
     */
    public void AnimateContent(View v, float offset) {

        float from = 0, to = 0;
        float from_scale = 0, to_scale = 0;
        Log.w("jab_prev", "" + prev_offset_scale);
        if (prev_offset > offset) {
            from = prev_offset;
            to = offset;

        } else {
            from = offset;
            to = prev_offset;
        }

        float scale_temp = 1 - offset; // 1-0.5=0.95
        if (scale_temp != 0)
            scale_temp /= 2; // 0.95/2=0.475
        scale_temp += 0.5; // 0.475+0.5=0.975

        if (prev_offset_scale > offset) {

            from_scale = prev_offset_scale;
            to_scale = scale_temp;
        } else {
            from_scale = scale_temp;
            to_scale = prev_offset_scale;
        }

        prev_offset *= 400f;
        to *= 450f;

        /*
         * from_scale/=2f; from_scale+=0.5f;
         *
         * to_scale/=2f; to_scale+=0.5f;
         */

        Log.w("jaba", from_scale + "----" + to_scale);
        AnimatorSet set = new AnimatorSet();
        // Using property animation
        ObjectAnimator animation1, animation3;
        SharedPreferences preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
        int lang = preferences.getInt("language", 0);
        if (lang == LANG_URDU)
        {
            animation1 = ObjectAnimator.ofFloat(v, "ScaleX", from_scale, to_scale);
             animation3 = ObjectAnimator.ofFloat(v, "translationX", -from, -to);
        }
        else {
            animation1 = ObjectAnimator.ofFloat(v, "ScaleX", from_scale, to_scale);
             animation3 = ObjectAnimator.ofFloat(v, "translationX", from, to);
        }

        ObjectAnimator animation2 = ObjectAnimator.ofFloat(v, "ScaleY", from_scale, to_scale);


        set.setDuration(10);
        set.playTogether(animation2, animation3, animation1);
        set.start();
        prev_offset_scale = scale_temp;
        offset *= 400f;
        prev_offset = offset;
        Log.w("jab_after", "" + prev_offset_scale);
    }

    public void showZoomIn(View v) {

        AnimatorSet set = new AnimatorSet();
        // Using property animation
        ObjectAnimator animation1 = ObjectAnimator.ofFloat(v, "ScaleX", 0.6f, 1f);
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(v, "ScaleY", 0.6f, 1f);
        ObjectAnimator animation3 = ObjectAnimator.ofFloat(v, "translationX", 200f, 1);

        set.setDuration(100);
        set.playTogether(animation1, animation2, animation3);
        set.start();

    }

    public void rotate(View fl_content2) {
        AnimatorSet set = new AnimatorSet();
        // Using property animation
        ObjectAnimator animation1 = ObjectAnimator.ofFloat(fl_content2, "rotateX", 1f, 2f);
        set.setDuration(1000);
        set.play(animation1);
        set.start();
    }


//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
////		if (db_subcategory.equalsIgnoreCase("search")) {
////			return false;
////		}
////		if (db_subcategory.equalsIgnoreCase("pronunciation")){
////
////		}
////		MenuInflater inflater = getMenuInflater();
////		inflater.inflate(R.menu.main, menu);
////		return super.onCreateOptionsMenu(menu);
//	}

    /* Called whenever we call invalidateOptionsMenu() */
//	@Override
//	public boolean onPrepareOptionsMenu(Menu menu) {
    // If the nav drawer is open, hide action items related to the content
    // view
//		boolean drawerOpen = mDrawerLayout.isDrawerOpen(fl_sidemenu);
//		menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
//		return super.onPrepareOptionsMenu(menu);
//	}
//
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        /*
         * if (mDrawerToggle.onOptionsItemSelected(item)) {
         *
         *
         *
         * return true; }
         */

        // Handle action buttons
        switch (item.getItemId()) {
            case R.id.action_websearch:
                // create intent to perform web search for this planet
                /*
                 * Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                 * intent.putExtra(SearchManager.QUERY, getSupportActionBar().getTitle());
                 * // catch event that there's no activity to handle intent if
                 * (intent.resolveActivity(getPackageManager()) != null) {
                 * startActivity(intent); } else { Toast.makeText(this,
                 * "app_not_available", Toast.LENGTH_LONG).show(); }
                 */
                showPopupList();
                return true;

            case R.id.action_home:

                finish();

                return true;

            case android.R.id.home:
                if (db_subcategory.equalsIgnoreCase("search")) {
                    onBackPressed();
                } else {
                    if (mDrawerLayout.getDrawerLockMode(fl_sidemenu) == DrawerLayout.LOCK_MODE_LOCKED_CLOSED) {
                        finish();
                    } else {
                        if (mDrawerLayout.isDrawerOpen(fl_sidemenu))

                            mDrawerLayout.closeDrawer(fl_sidemenu);
                        else
                            mDrawerLayout.openDrawer(fl_sidemenu);
                    }
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showPopupList() {

        // PopupMenu popup = new PopupMenu(getActivity(), v);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.popup_settings, null);
        // final ListView lv = (ListView) layout.findViewById(R.id.lv_letstalk);
        if (popup == null) {
            popup = new PopupWindow(layout, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            popup.setAnimationStyle(android.R.style.Animation_Toast);
            popup.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
            popup.setOutsideTouchable(true);
        }
        // popup.setFocusable(true);

        // popup.getMenu().add(0, i, 0, answer_Letstalks[i].getAnswer());

        // int child_count=getcurrentLayouChildCount();

        TextView tv_about = (TextView) layout.findViewById(R.id.tv_about_us);
        tv_about.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // Toast.makeText(ContainerActivity_two.this, "About Us",
                // Toast.LENGTH_SHORT).show();
                popup.dismiss();
//                AboutUs_Dialog dialog = new AboutUs_Dialog(ContainerActivity_two.this);
//                dialog.show();
                startActivity(new Intent(ContainerActivity_two.this, AboutUsActivity.class));
            }
        });
        TextView tv_speed = (TextView) layout.findViewById(R.id.tv_speed_control);
        tv_speed.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                popup.dismiss();
                SpeedControl_Dialog dialog = new SpeedControl_Dialog(ContainerActivity_two.this);
                dialog.show();
            }
        });
        if (db_subcategory.equalsIgnoreCase("pronunciation")) {
            tv_speed.setVisibility(View.GONE);
        }
        TextView tv_share = (TextView) layout.findViewById(R.id.tv_share);
        tv_share.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                popup.dismiss();
                String shareBody = "https://play.google.com/store/apps/details?id=com.qatariphrasebook.android";
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Try this out");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_using)));
            }
        });

        LinearLayout ll = (LinearLayout) layout.findViewById(R.id.ll_popup);
        /*
         * popup.setWidth(400); popup.setHeight(400);
         */
        popup.setOutsideTouchable(true);

        if (popup.isShowing())
            popup.dismiss();
        else
            popup.showAsDropDown(findViewById(R.id.search), 0, 30);

    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        // update the main content by replacing fragments

        // args.putInt(PlanetFragment.ARG_PLANET_NUMBER, position);
        if (position == 0) {
            Bundle args = new Bundle();
            args.putString("subcategory", db_subcategory);
            Fragment fragment = new Playlistfragment();
            fragment.setArguments(args);

//			FragmentManager fragmentManager = getFragmentManager();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
        } else if (position == 1) {

            Fragment fragment = new Searchlistfragment();
            tv_title.setVisibility(View.GONE);
            settings.setVisibility(View.GONE);
            home.setVisibility(View.GONE);
            favourites.setVisibility(View.VISIBLE);
            ll_search.setVisibility(View.VISIBLE);


            if (lang == 1) {
                edt_search.setHint("Chercher");
            } else {
                edt_search.setHint(R.string.Search);
            }

//			FragmentManager fragmentManager = getFragmentManager();

            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
        } else if (position == 2) {

            Fragment fragment = new Favouritefragment();
            if (lang == 1) {
                tv_title.setText("Favoris");
            } else {
                tv_title.setText("Favourites");
            }

//			FragmentManager fragmentManager = getFragmentManager();

            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();

        }
        if (position == 3) {

            Fragment fragment = new PrononciationFragment();

//			FragmentManager fragmentManager = getFragmentManager();

            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();

        }
        // update selected item and title, then close the drawer
        // mDrawerList.setItemChecked(position, true);
        setTitle(mTitle);
        mDrawerLayout.closeDrawer(fl_sidemenu);
    }

    public void loadListFromSideMenu(String title, String sub) {
        Fragment fragment = new Playlistfragment();
        Bundle args = new Bundle();
        db_subcategory = sub;
        args.putString("subcategory", db_subcategory);
        // args.putInt(PlanetFragment.ARG_PLANET_NUMBER, position);
        fragment.setArguments(args);

//		FragmentManager fragmentManager = getFragmentManager();
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
//        setLanguage();
        mTitle = title;
//        setTitle(mTitle);
        tv_title.setText(mTitle);
        mDrawerLayout.closeDrawer(fl_sidemenu);
    }

    public void loadPronunciationFromSideMEnu() {
        Fragment fragment = new PrononciationFragment();
        db_subcategory = "pronunciation";

//		FragmentManager fragmentManager = getFragmentManager();

        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
        mTitle = getString(R.string.pronounciation_guide);
//        setTitle(mTitle);
        tv_title.setText(mTitle);

        mDrawerLayout.closeDrawer(fl_sidemenu);
    }

    private void setsidemenu() {

//        if (db_subcategory.equals("pronunciation")) {
//            position = "14";
//        }
        LoadPosition();

        Log.e("fathima", " subcategory and position is -- " + db_subcategory + "   " + position);


        Fragment fragment = new SideMenuFragment(db_subcategory, position);

//		FragmentManager fragmentManager = getFragmentManager();
        getSupportFragmentManager().beginTransaction().replace(R.id.left_drawer, fragment).commit();
        mDrawerLayout.closeDrawer(fl_sidemenu);
    }

    private void LoadPosition() {
        switch (db_subcategory) {
            case "basics":
                position = "0";
                break;
            case "greetings":
                position = "0";
                break;
            case "expressions":
                position = "0";
                break;
            case "questions":
                position = "0";
                break;
            case "telephone":
                position = "0";
                break;
            case "feelings":
                position = "0";
                break;
            case "verbs":
                position = "0";
                break;
            case "sports":
                position = "1";
                break;
            case "numbers":
                position = "2";
                break;
            case "ordinate_numbers":
                position = "2";
                break;
            case "amounts":
                position = "2";
                break;
            case "time_and_dates":
                position = "3";
                break;
            case "telling_the_time":
                position = "3";
                break;
            case "days_of_the_week":
                position = "3";
                break;
            case "western_calender":
                position = "3";
                break;
            case "islamic_calender":
                position = "3";
                break;
            case "weather":
                position = "4";
                break;
            case "money_and_banks":
                position = "5";
                break;
            case "banks_money":
                position = "5";
                break;
            case "bills_and_cheques":
                position = "5";
                break;
            case "transportations":
                position = "6";
                break;
            case "directions":
                position = "6";
                break;
            case "bus_metro_train":
                position = "6";
                break;
            case "taxi":
                position = "6";
                break;
            case "rent_a_car":
                position = "6";
                break;
            case "emergency":
                position = "7";
                break;
            case "on_the_road":
                position = "7";
                break;
            case "problems":
                position = "7";
                break;
            case "authorities":
                position = "7";
                break;
            case "travel":
                position = "8";
                break;
            case "at_the_airport":
                position = "8";
                break;
            case "passport_customs":
                position = "8";
                break;
            case "food_and_drinks":
                position = "9";
                break;
            case "food_groceries":
                position = "9";
                break;
            case "drinks":
                position = "9";
                break;
            case "at_the_restaurants":
                position = "10";
                break;
            case "hotel":
                position = "11";
                break;
            case "checking_in":
                position = "11";
                break;
            case "checking_out":
                position = "11";
                break;
            case "shopping":
                position = "12";
                break;
            case "colors":
                position = "12";
                break;
            case "clothing":
                position = "12";
                break;
            case "stationary":
                position = "12";
                break;
            case "places_and_attractions":
                position = "13";
                break;
            case "sightseeing":
                position = "13";
                break;
            case "health":
                position = "14";
                break;
            case "parts_of_the_body":
                position = "14";
                break;
            case "doctors_pharmacy":
                position = "14";
                break;
            case "pronunciation":
                position = "15";
                break;
            default:
                position = "15";
                break;
        }

    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(fl_sidemenu);
    }

    public void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }

    }
}
