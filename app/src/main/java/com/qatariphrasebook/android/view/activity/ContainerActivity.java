package com.qatariphrasebook.android.view.activity;

import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.qatariphrasebook.android.R;
import com.qatariphrasebook.android.view.dialog.AboutUs_Dialog;
import com.qatariphrasebook.android.view.dialog.SpeedControl_Dialog;
import com.qatariphrasebook.android.view.fragment.CategoryFragment;
import com.qatariphrasebook.android.view.fragment.SideMenuFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ContainerActivity extends BaseActivity {

    private DrawerLayout mDrawerLayout;
    // private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private FrameLayout fl_sidemenu, fl_content;
    public static final int REQUEST_CODE_ASK_PERMISSIONS = 4999;
    private MenuView mv_search;
    public Toolbar toolbar;
    public PopupWindow popup;

    private boolean is_float;

    public String[] permissions = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO};
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private ImageView favourites;
    private ImageView search;
    private LinearLayout ll_language;
    private TextView tv_language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_container);

        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        // mDrawerList = (ListView) findViewById(R.id.left_drawer);
        fl_sidemenu = (FrameLayout) findViewById(R.id.left_drawer);
        fl_content = (FrameLayout) findViewById(R.id.content_frame);
        mv_search = (MenuView) findViewById(R.id.action_websearch);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        favourites = (ImageView) findViewById(R.id.favourites);
        search = (ImageView) findViewById(R.id.search);
        ll_language = (LinearLayout) findViewById(R.id.ll_language);
        ImageView hamburger = (ImageView) findViewById(R.id.img_hamburger);
        tv_language = (TextView) findViewById(R.id.tv_language);
        hamburger.setVisibility(View.GONE);


        setLanguage();

        favourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(ContainerActivity.this, ContainerActivity_two.class);
                i2.putExtra("Title", getString(R.string.Favourites));
                i2.putExtra("subcategory", "favourites");
                i2.putExtra("position", "0");
                startActivity(i2);
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ContainerActivity.this, ContainerActivity_two.class);
                i.putExtra("Title", getString(R.string.Search));
                i.putExtra("subcategory", "search");
                i.putExtra("position", "0");
                startActivity(i);

            }
        });

        ll_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLanguagePopupList();
            }
        });


        grantPermission(permissions);
        // set a custom shadow that overlays the main content when the drawer
        // opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                GravityCompat.START);

        // mDrawerList.setAdapter(new ArrayAdapter<String>(this,
        // R.layout.drawer_list_item, array_cat_names));

//        setsidemenu();
        // mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        setSupportActionBar(toolbar);
        // enable ActionBar app icon to behave as action to toggle nav drawer
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#203153")));
//        getSupportActionBar().setIcon(
//                getResources().getDrawable(R.drawable.action_logo));
        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon

        /*
         * mDrawerToggle = new ActionBarDrawerToggle( this, host Activity
         * mDrawerLayout, DrawerLayout object R.drawable.ic_drawer, nav drawer
         * image to replace 'Up' caret R.string.hello_world, "open drawer"
         * description for accessibility R.string.app_name "close drawer"
         * description for accessibility ) { public void onDrawerClosed(View
         * view) {
         *
         * invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu() //
         * showZoomIn(fl_content); // rotate( fl_content);
         * showHistory(fl_content,"translationX");
         * Toast.makeText(ContainerActivity.this, "closed",
         * Toast.LENGTH_SHORT).show(); }
         *
         *
         * public void onDrawerOpened(View drawerView) {
         *
         * invalidateOptionsMenu();
         *
         * // showZoomOut(fl_content); // creates call to onPrepareOptionsMenu()
         * showHistory(fl_content,"translationY");
         * Toast.makeText(ContainerActivity.this, "open",
         * Toast.LENGTH_SHORT).show(); }
         *
         *
         * }; mDrawerLayout.setDrawerListener(mDrawerToggle);
         */
        if (savedInstanceState == null) {
            selectItem(0);
        }

        // Fragment f=new PlayList_Fragment();

        // getFragmentManager().beginTransaction().replace(R.id.fl_container,
        // f).commit();

        // initUi();

        // registerUiEvents();
        /*
         *
         * LayoutInflater mInflater = LayoutInflater.from(this);
         *
         * /*View mCustomView = mInflater.inflate(R.layout.actionbar, null);
         * android.app.ActionBar mActionBar = getSupportActionBar();
         * mActionBar.setDisplayShowHomeEnabled(false);
         * mActionBar.setDisplayShowTitleEnabled(false);
         * mActionBar.setCustomView(mCustomView);
         * mActionBar.setDisplayShowCustomEnabled(true);
         *
         * Button btn_dropdown = (Button) mCustomView
         * .findViewById(R.id.btn_dropdown); btn_dropdown.setOnClickListener(new
         * OnClickListener() {
         *
         * @Override public void onClick(View v) { // TODO Auto-generated method
         * stub
         *
         * showPopupList(v); } });
         */
    }

    private void setLanguage() {
        SharedPreferences preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
        int lang = preferences.getInt("language", 0);
        if (lang == 1) {

            setLanguage("fr");
            tv_language.setText("FR");

        } else if (lang == 2) {
            setLanguage("hi");
            tv_language.setText("HI");
        } else if (lang == 3) {
            setLanguage("es");
            tv_language.setText("ES");
        } else if (lang == 4) {
            setLanguage("fil");
            tv_language.setText("FIL");
        } else if (lang == 5) {
            setLanguage("ur");
            tv_language.setText("UR");
        } else if (lang == 6) {
            setLanguage("ne");
            tv_language.setText("NP");
        }   else if (lang == 7) {
            setLanguage("zh");
            tv_language.setText("CN");
        }
        else if (lang == 8) {
            setLanguage("tr");
            tv_language.setText("TR");
        }
        else {

            setLanguage("en");
            tv_language.setText("EN");
        }


    }

    public void showHistory(View v, String anim) {

        AnimatorSet set = new AnimatorSet();
        // Using property animation
        ObjectAnimator animation = ObjectAnimator.ofFloat(v, anim, 1f, 200f);

        animation.setDuration(2000);
        set.play(animation);
        set.start();

    }

    public void showHistoryY(View v, String anim) {

        AnimatorSet set = new AnimatorSet();
        // Using property animation
        ObjectAnimator animation = ObjectAnimator.ofFloat(v, anim, 200f, 1f);

        animation.setDuration(2000);
        set.play(animation);
        set.start();

    }

    public void showZoomOut(View v) {

        AnimatorSet set = new AnimatorSet();
        // Using property animation
        ObjectAnimator animation1 = ObjectAnimator.ofFloat(v, "ScaleX", 1f,
                0.6f);
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(v, "ScaleY", 1f,
                0.6f);
        ObjectAnimator animation3 = ObjectAnimator.ofFloat(v, "translationX",
                1f, 200f);

        set.setDuration(100);
        set.playTogether(animation1, animation2, animation3);
        set.start();

    }

    /**
     * Not Using
     *
     * @param v
     */
    public void showZoomIn(View v) {

        AnimatorSet set = new AnimatorSet();
        // Using property animation
        ObjectAnimator animation1 = ObjectAnimator.ofFloat(v, "ScaleX", 0.6f,
                1f);
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(v, "ScaleY", 0.6f,
                1f);
        ObjectAnimator animation3 = ObjectAnimator.ofFloat(v, "translationX",
                200f, 1);

        set.setDuration(100);
        set.playTogether(animation1, animation2, animation3);
        set.start();

    }

    /**
     * Not Using
     *
     * @param fl_content2
     */
    public void rotate(View fl_content2) {
        AnimatorSet set = new AnimatorSet();
        // Using property animation
        ObjectAnimator animation1 = ObjectAnimator.ofFloat(fl_content2,
                "rotateX", 1f, 2f);
        set.setDuration(1000);
        set.play(animation1);
        set.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.main_search, menu);
//
//        MenuItem item = menu.findItem(R.id.action_language);
//        SpannableString spanString = new SpannableString(item.getTitle());
//        spanString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spanString.length(), 0);
//        item.setTitle(spanString);

        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content
        // view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(fl_sidemenu);
//        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
//        menu.findItem(R.id.action_favourite).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        /*
         * if (mDrawerToggle.onOptionsItemSelected(item)) { return true; }
         */
        // Handle action buttons
        switch (item.getItemId()) {
            case R.id.action_websearch:
                // create intent to perform web search for this planet
                /*
                 * Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                 * intent.putExtra(SearchManager.QUERY, getSupportActionBar().getTitle());
                 * // catch event that there's no activity to handle intent if
                 * (intent.resolveActivity(getPackageManager()) != null) {
                 * startActivity(intent); } else { Toast.makeText(this,
                 * "app_not_available", Toast.LENGTH_LONG).show(); }
                 */

                Intent i = new Intent(this, ContainerActivity_two.class);
                i.putExtra("Title", getString(R.string.Search));
                i.putExtra("subcategory", "search");
                startActivity(i);

                return true;

            case R.id.action_favourite:
                // create intent to perform web search for this planet
                /*
                 * Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                 * intent.putExtra(SearchManager.QUERY, getSupportActionBar().getTitle());
                 * // catch event that there's no activity to handle intent if
                 * (intent.resolveActivity(getPackageManager()) != null) {
                 * startActivity(intent); } else { Toast.makeText(this,
                 * "app_not_available", Toast.LENGTH_LONG).show(); }
                 */

                Intent i2 = new Intent(this, ContainerActivity_two.class);
                i2.putExtra("Title", getString(R.string.Favourites));
                i2.putExtra("subcategory", "favourites");
                startActivity(i2);

                return true;

            case R.id.action_language:
                showLanguagePopupList();
                return true;

//            case R.id.english:
//                preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
//                editor = preferences.edit();
//                editor.putInt("language", 0);
//                editor.commit();
//                startActivity(new Intent(ContainerActivity.this, ContainerActivity.class));
//                finish();
//
//
////                SharedPreferences preferences=getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
////                int lang=preferences.getInt("language",0);
////                Toast.makeText(this, lang+"", Toast.LENGTH_SHORT).show();
//                return true;

//            case R.id.french:
////                Toast.makeText(this, "French", Toast.LENGTH_SHORT).show();
//                preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
//                editor = preferences.edit();
//                editor.putInt("language", 1);
//                editor.commit();
//                startActivity(new Intent(ContainerActivity.this, ContainerActivity.class));
//                finish();
//                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showPopupList() {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.popup_language, null);
        if (popup == null) {
            popup = new PopupWindow(layout, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            popup.setAnimationStyle(android.R.style.Animation_Toast);
            popup.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
            popup.setOutsideTouchable(true);
        }

        TextView tv_english = (TextView) layout.findViewById(R.id.tv_english);
        tv_english.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                popup.dismiss();
                preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
                editor = preferences.edit();
                editor.putInt("language", 0);
                editor.commit();
                startActivity(new Intent(ContainerActivity.this, ContainerActivity.class));
                finish();

            }
        });
        TextView tv_french = (TextView) layout.findViewById(R.id.tv_french);
        tv_french.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                popup.dismiss();
                preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
                editor = preferences.edit();
                editor.putInt("language", 1);
                editor.commit();
                startActivity(new Intent(ContainerActivity.this, ContainerActivity.class));
                finish();
            }
        });

        TextView tv_hindi =  layout.findViewById(R.id.tv_hindi);
        tv_hindi.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                popup.dismiss();
                preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
                editor = preferences.edit();
                editor.putInt("language", 2);
                editor.commit();
                startActivity(new Intent(ContainerActivity.this, ContainerActivity.class));
                finish();
            }
        });
        TextView tv_spanish =  layout.findViewById(R.id.tv_spanish);
        tv_spanish.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                popup.dismiss();
                preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
                editor = preferences.edit();
                editor.putInt("language", 3);
                editor.commit();
                startActivity(new Intent(ContainerActivity.this, ContainerActivity.class));
                finish();
            }
        });

        TextView tv_filipino =  layout.findViewById(R.id.tv_filipino);
        tv_filipino.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                popup.dismiss();
                preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
                editor = preferences.edit();
                editor.putInt("language", 4);
                editor.commit();
                startActivity(new Intent(ContainerActivity.this, ContainerActivity.class));
                finish();
            }
        });


TextView tv_chinese =  layout.findViewById(R.id.tv_chinese);
        tv_chinese.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                popup.dismiss();
                preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
                editor = preferences.edit();
                editor.putInt("language", 7);
                editor.commit();
                startActivity(new Intent(ContainerActivity.this, ContainerActivity.class));
                finish();
            }
        });


TextView tv_turkish =  layout.findViewById(R.id.tv_turkish);
        tv_turkish.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                popup.dismiss();
                preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
                editor = preferences.edit();
                editor.putInt("language", 8);
                editor.commit();
                startActivity(new Intent(ContainerActivity.this, ContainerActivity.class));
                finish();
            }
        });



        popup.setOutsideTouchable(true);
        if (popup.isShowing())
            popup.dismiss();
        else
            popup.showAsDropDown(findViewById(R.id.ll_language), 0, 30);

    }


    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        // update the main content by replacing fragments
        Fragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        // args.putInt(PlanetFragment.ARG_PLANET_NUMBER, position);
        fragment.setArguments(args);

//		FragmentManager fragmentManager = getFragmentManager();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment).commit();

        // update selected item and title, then close the drawer
        // mDrawerList.setItemChecked(position, true);
        setTitle("Qatari PhraseBook");
        mDrawerLayout.closeDrawer(fl_sidemenu);
    }

    private void setsidemenu() {
        Fragment fragment = new SideMenuFragment("basics", "0");

//		FragmentManager fragmentManager = getSupportFragmentManager();
        getSupportFragmentManager().beginTransaction().replace(R.id.left_drawer, fragment)
                .commit();
        mDrawerLayout.closeDrawer(fl_sidemenu);
    }

    public void enableDrawer(boolean enable) {
        if (enable) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        } else {
            mDrawerLayout
                    .setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    public void grantPermission(String[] permissions) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return;

        List<String> requiredPermissions = new ArrayList<>();
        for (int i = 0; i < permissions.length; i++) {
            if (checkSelfPermission(permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                requiredPermissions.add(permissions[i]);
            }
        }

        String[] stockArr = new String[requiredPermissions.size()];
        stockArr = requiredPermissions.toArray(stockArr);

        if (stockArr == null || stockArr.length == 0)
            return;

        requestPermissions(stockArr, REQUEST_CODE_ASK_PERMISSIONS);
    }

    public boolean hasPermissions() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;
        for (int i = 0; i < permissions.length; i++) {
            if (checkSelfPermission(permissions[i]) != PackageManager.PERMISSION_GRANTED)
                return false;

        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setLanguage();
        selectItem(0);
//        setsidemenu();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
        finish();
    }
}
