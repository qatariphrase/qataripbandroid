package com.qatariphrasebook.android.view.dialog;

import com.qatariphrasebook.android.R;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.TextView;

import java.util.Locale;

public class AboutUs_Dialog extends Dialog {

    Context context;
    TextView tv_link;

    public AboutUs_Dialog(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_aboutus);
        tv_link = (TextView) findViewById(R.id.tv_link_dialog_aboutus);
        WebView wv_prono = (WebView) findViewById(R.id.wv_pronounce);

        SharedPreferences preferences = getContext().getSharedPreferences("temp", getContext().MODE_PRIVATE);
        int lang = preferences.getInt("language", 0);
        if (lang == 1) {

            Locale locale = new Locale("fr");
            Resources res = getContext().getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = locale;
            res.updateConfiguration(conf, dm);
        } else {

            Locale locale = new Locale("en");
            Resources res = getContext().getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = locale;
            res.updateConfiguration(conf, dm);

        }
//        wv_prono.loadDataWithBaseURL(null, String.valueOf((R.string.about)), "text/html", "UTF-8", null);

//        wv_prono.loadDataWithBaseURL("file:///android_res/drawable/",
//                "<img src='prono.jpg' />", "text/html", "utf-8", null);
//
//        wv_prono.getSettings().setBuiltInZoomControls(true);
//        wv_prono.getSettings().setLoadWithOverviewMode(true);
//        wv_prono.getSettings().setUseWideViewPort(true);


        tv_link.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://qatar.sfs.georgetown.edu"));
                context.startActivity(browserIntent);
            }
        });
    }
}
