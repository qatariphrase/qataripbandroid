package com.qatariphrasebook.android.view.fragment;

import static com.qatariphrasebook.android.view.utils.Constants.LANG_FRENCH;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_HINDI;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.qatariphrasebook.android.R;
import com.qatariphrasebook.android.db.MyDatabase;
import com.qatariphrasebook.android.domain.Audio_File;
import com.qatariphrasebook.android.view.activity.ContainerActivity_two;
import com.qatariphrasebook.android.view.adapter.PlayListAdapter;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class Playlistfragment extends Fragment implements MediaPlayer.OnCompletionListener {

    ListView lv_playlist;
    ImageView iv_play;
    TextView tv_english, tv_arabic, tv_arabic_in_english;
    PlayListAdapter adapter;
    ArrayList<Audio_File> arrayList;
    int previous_position = -1;
    String subcategory;
    private int lang;
    MediaPlayer player;
    boolean isFinished;
    boolean ispaused;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.activity_playlist, container,
                false);
        subcategory = getArguments().getString("subcategory");
        SharedPreferences preferences = getActivity().getSharedPreferences("temp", getActivity().MODE_PRIVATE);
        lang = preferences.getInt("language", 0);
        return view;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);

        initUI();
        populateList();

        lv_playlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                // TODO Auto-generated method stub


                String title;
                if (lang == LANG_FRENCH) {
                    title = arrayList.get(0).getFrench_title();
                } else if (lang == LANG_HINDI)
                    title = arrayList.get(0).getHindi_title();
                else {
                    title = arrayList.get(0).getEng_title();
                }


                tv_english.setText(title);
                tv_arabic.setText(arrayList.get(position).getArabic_title());
                tv_arabic_in_english.setText(arrayList.get(position).getArabi_in_english());

                if (previous_position == position) {
                    if (iv_play.getTag().toString().equalsIgnoreCase("play")) {
                        Log.e("fathimakp", "inside 1.a");

                        iv_play.setImageResource(R.drawable.pause_big);
                        iv_play.setTag("pause");
                        playThisFile(arrayList.get(position).getFile_name(), arrayList.get(position).getSub_category(), "a");

                    } else {
                        Log.e("fathimakp", "inside 1.b");
                        iv_play.setImageResource(R.drawable.play_big);
                        iv_play.setTag("play");

                        if (isFinished) {
                            iv_play.setTag("pause");
                            playThisFile(arrayList.get(position).getFile_name(), arrayList.get(position).getSub_category(), "a");
                        } else {
                            player.pause();
                            ispaused = true;
                        }
                    }

                } else {
                    Log.e("fathimakp", "inside 1.c");

                    if (player != null) {
                        Log.e("fathimakp", "player not null");
//                        player.stop();
                    }

                    iv_play.setImageResource(R.drawable.pause_big);
                    iv_play.setTag("pause");
                    playThisFile(arrayList.get(position).getFile_name(), arrayList.get(position).getSub_category(), "c");
                }

                adapter.setPlayState(position, iv_play.getTag().toString());
                previous_position = position;

            }
        });


    }


    private void populateList() {

        arrayList = createList();
        adapter = new PlayListAdapter(arrayList, getActivity(), lang);
        lv_playlist.setAdapter(adapter);

        lv_playlist.setSelector(R.color.list_selection);
        lv_playlist.setSelection(0);

        if (lang == 1) {
            tv_english.setText(arrayList.get(0).getFrench_title());
        } else {
            tv_english.setText(arrayList.get(0).getEng_title());
        }

        tv_arabic.setText(arrayList.get(0).getArabic_title());
        tv_arabic_in_english.setText(arrayList.get(0).getArabi_in_english());
    }

    private void initUI() {
        // TODO Auto-generated method stub
        lv_playlist = (ListView) getView().findViewById(R.id.lv_playlist);
        tv_arabic = (TextView) getView().findViewById(R.id.tv_arabic_main);
        tv_english = (TextView) getView().findViewById(R.id.tv_english_main);
        tv_arabic_in_english = (TextView) getView().findViewById(R.id.tv_arabic_in_english);
        iv_play = (ImageView) getView().findViewById(R.id.iv_play_main);
        iv_play.setTag("play");


    }

    private ArrayList<Audio_File> createList() {


        MyDatabase db = ((ContainerActivity_two) getActivity()).getDB();

        if (subcategory.equals("basics") ||
                (subcategory.equals("numbers")) ||
                (subcategory.equals("time_and_dates")) ||
                (subcategory.equals("weather")) ||
                (subcategory.equals("money_and_banks")) ||
                (subcategory.equals("transportations")) ||
                (subcategory.equals("emergency")) ||
                (subcategory.equals("travel")) ||
                (subcategory.equals("food_and_drinks")) ||
                (subcategory.equals("at_the_restaurants")) ||
                (subcategory.equals("hotel")) ||
                (subcategory.equals("shopping")) ||
                (subcategory.equals("places_and_attractions")) ||
                (subcategory.equals("health"))) {
            Cursor c = db.getCategory(subcategory);
            return parseCursor(c);

        } else {
            Cursor c = db.getSubCategory(subcategory);
            return parseCursor(c);
        }
    }


    private ArrayList<Audio_File> parseCursor(Cursor c) {


        ArrayList<Audio_File> data_list = new ArrayList<Audio_File>();

        while (c.moveToNext()) {
//            Log.w("DB---> ", c.getString(0));//id
//            Log.w("DB---> ", c.getString(1));//English
//            Log.w("DB---> ", c.getString(2));//French
//            Log.w("DB---> ", c.getString(3));//Hindi
//            Log.w("DB---> ", c.getString(4));//Nepali
//            Log.w("DB---> ", c.getString(5));//Spanish
//            Log.w("DB---> ", c.getString(6));//Filipino
//            Log.w("DB---> ", c.getString(7));//Urdu
//            Log.w("DB---> ", c.getString(8));//Arabic
//            Log.w("DB---> ", c.getString(9));//ArabicEnglish
//            Log.w("DB---> ", c.getString(10));//Chinese
//            Log.w("DB---> ", c.getString(11));//Turkish
            if (lang == 5) {
                if (!c.getString(7).trim().isEmpty())
                    data_list.add(new Audio_File(c.getString(0), c.getString(1), c.getString(2), c.getString(3),
                            c.getString(4), c.getString(5),
                            c.getString(6), c.getString(7), c.getString(8), c.getString(9),c.getString(10),c.getString(11),
                            c.getString(12) + ".ogg", c.getString(13), c.getString(14), c.getString(15)));
            } else {
                if (!c.getString(2).trim().isEmpty())
                    data_list.add(new Audio_File(c.getString(0), c.getString(1), c.getString(2), c.getString(3),
                            c.getString(4), c.getString(5),
                            c.getString(6), c.getString(7), c.getString(8), c.getString(9),c.getString(10),c.getString(11),
                            c.getString(12) + ".ogg", c.getString(13), c.getString(14), c.getString(15)));

            }

        }
        return data_list;

    }


    /**
     * Plays a particular file
     *
     * @param filename    audio file name
     * @param subcategory subcategory in which the audio belongs to
     * @param c
     */
    public void playThisFile(String filename, String subcategory, String c) {
        Log.e("fathimakp", "inside play this file");
        if (player != null) {
            Log.e("fathimakp", "inside play this file---player not null");
//
            if (ispaused || isFinished) {
                Log.e("fathimakp", "inside play this file---is paused or finish");

////            if (ispaused || finished) {
//
                if (isFinished) {
                    Log.e("fathimakp", "inside play this file---player not null---isfnished");
//
                    playSound(filename, subcategory);
//
                } else {
                    Log.e("fathimakp", "inside play this file---player not null---ispasuded");
                    if (c.equals("a")) {
                        player.start();
                        ispaused = false;
                        isFinished = false;
                    } else {
                        ispaused = false;
                        isFinished = true;
                        playSound(filename, subcategory);

                    }
                }
            } else {
                Log.e("fathimakp", "inside play this file---player not null else");

//                player.stop();
                ispaused = false;
                playSound(filename, subcategory);
            }

//
        } else {
            Log.e("fathimakp", "inside play this file---player null");
            playSound(filename, subcategory);

        }
    }


    /**
     * Retrieves audio file from asset. Copy to sd card. Returns that file
     *
     * @param filename    audio file name
     * @param subcategory subcategory in which the audio belongs to
     * @return File
     */
    private File play(String filename, String subcategory) {
        // File file = //...


        File file = null;
        try {
            File newFolder = new File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "PhraseBook");
            if (!newFolder.exists()) {
                newFolder.mkdir();
//                File noMedia = new File(newFolder.getAbsolutePath() + "/.nomedia");
//                noMedia.mkdirs();
//                noMedia.createNewFile();
            }
            try {
                file = new File(newFolder, filename.substring(0, filename.length() - 4) + ".ogg");
                file.createNewFile();
            } catch (Exception ex) {
                System.out.println("ex: " + ex);
            }
        } catch (Exception e) {
            System.out.println("e: " + e);
        }
        FileDescriptor fd = null;
        try {
            // fd = getAssets().openFd("3.ogg").getFileDescriptor();

            String path = subcategory + "/" + filename;
            InputStream is = getActivity().getAssets().open(path);
            long file_length = getActivity().getAssets().openFd(path).getLength();
            if (getAvailableSpaceInBytes() > file_length) {
                OutputStream outputStream = new FileOutputStream(file);
                IOUtils.copy(is, outputStream);
                outputStream.close();
            } else {
                Toast.makeText(getActivity(), "No space in Sdcard. Free some space First", Toast.LENGTH_SHORT).show();
            }


        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }


        return file;
    }

    /**
     * z
     * Invokes the SoundStreamAudioPlayer to play the file
     *
     * @param filename    audio file name
     * @param subcategory subcategory in which the audio belongs to
     */
    public void playSound(String filename, String subcategory) {
        File file = play(filename, subcategory);
        String path = file.getAbsolutePath();
        String audioPath = subcategory + "/" + filename;
        Log.e("fathimakp", "path is " + audioPath);
        playAudio(audioPath);
    }

    public void playAudio(String path) {
        float tempo = getPreference();
        if (player != null) {
            player.release();
        }
        player = new MediaPlayer();

        try {
            if (player.isPlaying()) {
                Log.e("fathimakp", "inside play beep---playing is true");
                player.stop();
                player.release();
                player = new MediaPlayer();
            }


            AssetFileDescriptor descriptor = getActivity().getAssets().openFd(path);
            Log.e("fathimakp", "audio path is " + path);
            player.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();

            player.prepare();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                player.setPlaybackParams(player.getPlaybackParams().setSpeed(tempo));
            }
            player.setVolume(1f, 1f);
            player.start();
            ispaused = false;
            isFinished = false;
        } catch (Exception e) {
            e.printStackTrace();
        }

        player.setOnCompletionListener(this);
    }

    /**
     * Checks the available space in sdcard
     *
     * @return long available space
     */
    public long getAvailableSpaceInBytes() {
        long availableSpace = -1L;
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        availableSpace = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();

        //    Toast.makeText(getActivity(), ""+availableSpace, 0).show();

        return availableSpace;
    }

    /**
     * Get the audio play speed set by user from preference
     *
     * @return float speed
     */
    private float getPreference() {

        SharedPreferences preference = getActivity().getSharedPreferences("Speed", Context.MODE_PRIVATE);
        float value = preference.getFloat("speed", 0.8f);
        return value;
    }

    @Override
    public void onCompletion(MediaPlayer mplayer) {
        adapter.setPlayState(previous_position, "play");
        player.stop();
        player.release();
        isFinished = true;
    }

}
