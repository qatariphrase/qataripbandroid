package com.qatariphrasebook.android.view.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupExpandListener;

import androidx.fragment.app.Fragment;

import com.qatariphrasebook.android.R;
import com.qatariphrasebook.android.view.activity.ContainerActivity_two;
import com.qatariphrasebook.android.view.adapter.ExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SideMenuFragment extends Fragment {


    private int position;
    private String db_subcategory;
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    int[] cat_image_array;
    HashMap<String, List<String>> listDataChild;
    private int lastExpandedPosition = -1;

    public SideMenuFragment(String db_subcategory, String position) {
        this.db_subcategory = db_subcategory;
        this.position = Integer.parseInt(position);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        initUI();

    }

    private void initUI() {
        // TODO Auto-generated method stub
        expListView = (ExpandableListView) getView().findViewById(R.id.lvExp);
        expListView.setGroupIndicator(null);
        prepareListData();

        listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild, cat_image_array);


        // setting list adapter
        expListView.setAdapter(listAdapter);
        if (!(db_subcategory.equals("favourites") || db_subcategory.equals("search")))
            expListView.expandGroup(position);


        expListView.setOnGroupClickListener(new OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
//                parent.collapseGroup(position);

//                expListView.collapseGroup(position);
//                position = -1;

                // TODO Auto-generated method stub
//                String subcategory = null;

//                if (groupPosition == 14) {
//                    ((ContainerActivity_two) getActivity()).loadPronunciationFromSideMEnu();
//                    ((ContainerActivity_two) getActivity()).closeDrawer();
//                }else {
//                    switch (groupPosition) {
//                        case 0:
//                            subcategory = "basics";
//                            break;
//                        case 1:
//                            subcategory = "numbers";
//                            break;
//                        case 2:
//                            subcategory = "time_and_dates";
//                            break;
//                        case 3:
//                            subcategory = "weather";
//                            break;
//                        case 4:
//                            subcategory = "money_and_banks";
//                            break;
//                        case 5:
//                            subcategory = "transportations";
//                            break;
//                        case 6:
//                            subcategory = "emergency";
//                            break;
//                        case 7:
//                            subcategory = "travel";
//                            break;
//                        case 8:
//                            subcategory = "food_and_drinks";
//                            break;
//                        case 9:
//                            subcategory = "at_the_restaurants";
//                            break;
//                        case 10:
//                            subcategory = "hotel";
//                            break;
//                        case 11:
//                            subcategory = "shopping";
//                            break;
//                        case 12:
//                            subcategory = "places_and_attractions";
//                            break;
//                        case 13:
//                            subcategory = "health";
//                            break;
//                    }
//                    ((ContainerActivity_two) getActivity()).loadListFromSideMenu(listDataHeader.get(groupPosition), subcategory);
//                    ((ContainerActivity_two) getActivity()).closeDrawer();
//                }


                return false;
            }
        });


        expListView.setOnChildClickListener(new OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub

                String sub = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);
                Log.e("fathima", "group and child is " + groupPosition + " " + childPosition);

                if (groupPosition == 15) {
                    if (childPosition == 0) {
                        ((ContainerActivity_two) getActivity()).loadPronunciationFromSideMEnu();
                        ((ContainerActivity_two) getActivity()).closeDrawer();
                    }
                } else {
                    if (groupPosition == 0) {
                        if (childPosition == 0) {
                            sub = "greetings";
                        } else if(childPosition == 1){
                            sub = "Expressions";
                        }
                        else if (childPosition == 2) {
                            sub = "Questions";
                        } else if (childPosition == 3) {
                            sub = "Telephone";
                        } else if (childPosition == 4) {
                            sub = "Feelings";
                        } else if (childPosition == 5) {
                            sub = "Verbs";
                        }
                    }

                    if (groupPosition == 1) {
                        if (childPosition == 0) {
                            sub = "sports";
                        }
                    }

                    if (groupPosition == 2) {
                        if (childPosition == 0) {
                            sub = "numbers";
                        } else if (childPosition == 1) {
                            sub = "Ordinate Numbers";
                        } else if (childPosition == 2) {
                            sub = "Amounts";
                        }
                    }

                    if (groupPosition == 3) {
                        if (childPosition == 0) {
                            sub = "Telling the Time";
                        } else if (childPosition == 1) {
                            sub = "Days of the week";
                        } else if (childPosition == 2) {
                            sub = "Western Calendar";
                        } else if (childPosition == 3) {
                            sub = "Islamic Calendar";
                        }
                    }

                    if (groupPosition == 4) {
                        if (childPosition == 0) {
                            sub = "weather";
                        }
                    }

                    if (groupPosition == 5) {
                        if (childPosition == 0) {
                            sub = "Banks Money";
                        } else if (childPosition == 1) {
                            sub = "Bills and Cheques";
                        }
                    }

                    if (groupPosition == 6) {
                        if (childPosition == 0) {
                            sub = "Directions";
                        } else if (childPosition == 1) {
                            sub = "Bus Metro Train";
                        } else if (childPosition == 2) {
                            sub = "Taxi";
                        } else if (childPosition == 3) {
                            sub = "Rent a car";
                        }
                    }

                    if (groupPosition == 7) {
                        if (childPosition == 0) {
                            sub = "Emergency";
                        } else if (childPosition == 1) {
                            sub = "on the road";
                        } else if (childPosition == 2) {
                            sub = "problems";
                        } else if (childPosition == 3) {
                            sub = "authorities";
                        }
                    }

                    if (groupPosition == 8) {
                        if (childPosition == 0) {
                            sub = "at the airport";
                        } else if (childPosition == 1) {
                            sub = "passport customs";
                        }
                    }


                    if (groupPosition == 9) {
                        if (childPosition == 0) {
                            sub = "food groceries";
                        } else if (childPosition == 1) {
                            sub = "drinks";
                        }
                    }

                    if (groupPosition == 10) {
                        if (childPosition == 0) {
                            sub = "at the restaurants";
                        }
                    }

                    if (groupPosition == 11) {
                        if (childPosition == 0) {
                            sub = "checking in";
                        } else if (childPosition == 1) {
                            sub = "checking out";
                        }
                    }

                    if (groupPosition == 12) {
                        if (childPosition == 0) {
                            sub = "shopping";
                        } else if (childPosition == 1) {
                            sub = "colors";
                        } else if (childPosition == 2) {
                            sub = "clothing";
                        } else if (childPosition == 3) {
                            sub = "stationary";
                        }
                    }


                    if (groupPosition == 13) {
                        if (childPosition == 0) {
                            sub = "sightseeing";
                        }
                    }

                    if (groupPosition == 14) {
                        if (childPosition == 0) {
                            sub = "parts of the body";
                        } else if (childPosition == 1) {
                            sub = "doctors pharmacy";
                        }
                    }


                    sub = sub.replaceAll(",", "");
                    sub = sub.replaceAll(" ", "_");


                    ((ContainerActivity_two)

                            getActivity()).

                            loadListFromSideMenu(listDataChild.get(listDataHeader.get(groupPosition)).

                                    get(childPosition), sub.

                                    toLowerCase());
                    ((ContainerActivity_two)

                            getActivity()).

                            closeDrawer();
                }
                Log.e("fathima", "side menu title and subcategory" + listDataChild.get(listDataHeader.get(groupPosition)).

                        get(childPosition) + "   " + sub.toLowerCase());

                return false;
            }
        });


        expListView.setOnGroupExpandListener(new

                                                     OnGroupExpandListener() {

                                                         @Override
                                                         public void onGroupExpand(int groupPosition) {


                                                             if (lastExpandedPosition != -1
                                                                     && groupPosition != lastExpandedPosition) {
                                                                 expListView.collapseGroup(lastExpandedPosition);
                                                                 if (groupPosition == 15) {
                                                                     ((ContainerActivity_two) getActivity()).loadPronunciationFromSideMEnu();
                                                                     ((ContainerActivity_two) getActivity()).closeDrawer();
                                                                 }
                                                             } else if
                                                             (groupPosition == 15) {
                                                                 ((ContainerActivity_two) getActivity()).loadPronunciationFromSideMEnu();
                                                                 ((ContainerActivity_two) getActivity()).closeDrawer();
                                                             }

                                                             lastExpandedPosition = groupPosition;
                                                         }
                                                     });

    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.layout_sidemenu, container,
                false);
        return view;


    }

    ;


    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        cat_image_array = new int[]{R.drawable.nav_basics, R.drawable.nav_sports, R.drawable.nav_numbers, R.drawable.nav_time_dates, R.drawable.nav_weather, R.drawable.nav_money_banks, R.drawable.nav_transportations
                , R.drawable.nav_emergency, R.drawable.nav_travel, R.drawable.nav_food_drinks, R.drawable.nav_at_the_resturants, R.drawable.nav_hotel, R.drawable.nav_shopping, R.drawable.nav_places_attraction, R.drawable.nav_health, R.drawable.nav_pronounciation_guid};

        listDataHeader.add(getString(R.string.basics));
        listDataHeader.add(getString(R.string.sports));
        listDataHeader.add(getString(R.string.Numbers));

        listDataHeader.add(getString(R.string.timeDate));
        listDataHeader.add(getString(R.string.weather));
        listDataHeader.add(getString(R.string.moneyBank));
        listDataHeader.add(getString(R.string.transportation));
        listDataHeader.add(getString(R.string.emergency));
        listDataHeader.add(getString(R.string.travel));
        listDataHeader.add(getString(R.string.foodDrink));
        listDataHeader.add(getString(R.string.at_the_restaurants));
        listDataHeader.add(getString(R.string.hotel));
        listDataHeader.add(getString(R.string.shopping));
        listDataHeader.add(getString(R.string.places_and_attractions));
        listDataHeader.add(getString(R.string.health));
        listDataHeader.add(getString(R.string.pronounciation_guide));

        // Adding child data
        List<String> cat_1 = new ArrayList<String>();
        cat_1.add(getString(R.string.Greetings));
        cat_1.add(getString(R.string.Expressions));
        cat_1.add(getString(R.string.Questions));
        cat_1.add(getString(R.string.Telephone));
        cat_1.add(getString(R.string.Feelings));
        cat_1.add(getString(R.string.Verbs));

        List<String> cat_2 = new ArrayList<String>();

        cat_2.add(getString(R.string.Numbers));
        cat_2.add(getString(R.string.Ordinate_Numbers));
        cat_2.add(getString(R.string.Amounts));

        List<String> cat_3 = new ArrayList<String>();
        cat_3.add(getString(R.string.Telling_the_Time));
        cat_3.add(getString(R.string.Days_of_the_week));
        cat_3.add(getString(R.string.Western_Calendar));
        cat_3.add(getString(R.string.Islamic_Calendar));


        List<String> cat_4 = new ArrayList<String>();
        cat_4.add(getString(R.string.weather));


        List<String> cat_5 = new ArrayList<String>();
        cat_5.add(getString(R.string.Banks_Money));
        cat_5.add(getString(R.string.Bills_and_Cheques));


        List<String> cat_6 = new ArrayList<String>();
        cat_6.add(getString(R.string.Directions));
        cat_6.add(getString(R.string.Bus_Metro_Train));
        cat_6.add(getString(R.string.Taxi));
        cat_6.add(getString(R.string.Rent_a_car));

        List<String> cat_7 = new ArrayList<String>();
        cat_7.add(getString(R.string.Emergency));
        cat_7.add(getString(R.string.on_the_road));

        cat_7.add(getString(R.string.Problems));
        cat_7.add(getString(R.string.Authorities));
        List<String> cat_8 = new ArrayList<String>();
        cat_8.add(getString(R.string.At_the_Airport));
        cat_8.add(getString(R.string.Passport_Customs));

        List<String> cat_9 = new ArrayList<String>();
        cat_9.add(getString(R.string.Food_Groceries));
        cat_9.add(getString(R.string.Drinks));

        List<String> cat_10 = new ArrayList<String>();
        cat_10.add(getString(R.string.at_the_restaurants));

        List<String> cat_11 = new ArrayList<String>();

        cat_11.add(getString(R.string.Checkin_In));
        cat_11.add(getString(R.string.Checking_Out));

        List<String> cat_12 = new ArrayList<String>();
        cat_12.add(getString(R.string.Shopping));
        cat_12.add(getString(R.string.Colors));
        cat_12.add(getString(R.string.Clothing));
        cat_12.add(getString(R.string.Stationary));
        List<String> cat_13 = new ArrayList<String>();
        cat_13.add(getString(R.string.Sightseeing));


        List<String> cat_14 = new ArrayList<String>();
        cat_14.add(getString(R.string.Parts_of_the_Body));
        cat_14.add(getString(R.string.Doctors_Pharmacy));

        List<String> cat_15 = new ArrayList<String>();
        cat_15.add(getString(R.string.pronounciation_guide));

        List<String> cat_16 = new ArrayList<String>();
        cat_16.add(getString(R.string.sports));



        listDataChild.put(listDataHeader.get(0), cat_1); // Header, Child data
        listDataChild.put(listDataHeader.get(1), cat_16); // Header, Child data
        listDataChild.put(listDataHeader.get(2), cat_2);
        listDataChild.put(listDataHeader.get(3), cat_3);
        listDataChild.put(listDataHeader.get(4), cat_4);
        listDataChild.put(listDataHeader.get(5), cat_5);
        listDataChild.put(listDataHeader.get(6), cat_6);
        listDataChild.put(listDataHeader.get(7), cat_7);
        listDataChild.put(listDataHeader.get(8), cat_8);
        listDataChild.put(listDataHeader.get(9), cat_9);
        listDataChild.put(listDataHeader.get(10), cat_10);
        listDataChild.put(listDataHeader.get(11), cat_11);
        listDataChild.put(listDataHeader.get(12), cat_12);
        listDataChild.put(listDataHeader.get(13), cat_13);
        listDataChild.put(listDataHeader.get(14), cat_14);
        listDataChild.put(listDataHeader.get(15), cat_15);

    }

}

