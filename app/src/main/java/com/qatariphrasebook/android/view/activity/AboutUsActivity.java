package com.qatariphrasebook.android.view.activity;

import static com.qatariphrasebook.android.view.utils.Constants.LANG_CHINESE;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_FILIPINO;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_FRENCH;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_HINDI;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_NEPALI;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_SPANISH;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_TURKISH;
import static com.qatariphrasebook.android.view.utils.Constants.LANG_URDU;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.qatariphrasebook.android.R;

public class AboutUsActivity extends AppCompatActivity {

    public Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        ImageView back = (ImageView) findViewById(R.id.img_hamburger);
        TextView tv_title = (TextView) findViewById(R.id.tv_title);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        SharedPreferences preferences = getSharedPreferences("temp", getApplicationContext().MODE_PRIVATE);
        int lang = preferences.getInt("language", 0);


        WebView webView = findViewById(R.id.webView);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.setInitialScale(1);

        if (lang == LANG_FRENCH) {
            tv_title.setText("À propos de nous");
            webView.loadUrl("file:///android_asset/phrase_fr.html");
        } else if(lang == LANG_HINDI){
            tv_title.setText("हमारे बारे में");
            webView.loadUrl("file:///android_asset/phrase_hi.html");
        }
        else if(lang == LANG_SPANISH){
            tv_title.setText("spain");
            webView.loadUrl("file:///android_asset/phrase_es.html");
        }
        else if(lang == LANG_NEPALI){
            tv_title.setText("nepal");
            webView.loadUrl("file:///android_asset/phrase_np.html");
        }
        else if(lang == LANG_FILIPINO){
            tv_title.setText("filipino");
            webView.loadUrl("file:///android_asset/phrase_fil.html");
        }
        else if(lang == LANG_URDU){
            tv_title.setText("urdu");
            webView.loadUrl("file:///android_asset/phrase_ur.html");
        }
        else if(lang == LANG_CHINESE){
            tv_title.setText("china");
            webView.loadUrl("file:///android_asset/phrase_cn.html");
        } else if(lang == LANG_TURKISH){
            tv_title.setText("turkey");
            webView.loadUrl("file:///android_asset/phrase_tr.html");
        }
        else {
            tv_title.setText("About Us");
            webView.loadUrl("file:///android_asset/phrase.html");
        }


    }
}
