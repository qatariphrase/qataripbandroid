package com.qatariphrasebook.android.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

import com.qatariphrasebook.android.R;

public class SpeedControl_Dialog extends Dialog {

    Context context;

    TextView tv_text;
    SeekBar seekbar;
    Button btn_ok;
    float temp_value;

    private static final String decimalFormatStr = "0.00";

    public SpeedControl_Dialog(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_speed);


        tv_text = (TextView) findViewById(R.id.tv_seek_value);
        TextView textView1 = (TextView) findViewById(R.id.textView1);
        seekbar = (SeekBar) findViewById(R.id.seekBar_speed);
        btn_ok = (Button) findViewById(R.id.btn_dialog_seek_ok);

        seekbar.setMax(100);
        getPreference();

        SharedPreferences preferences = context.getSharedPreferences("temp", getContext().MODE_PRIVATE);
        int lang = preferences.getInt("language", 0);
        if (lang == 1) {
            textView1.setText("contrôle de vitesse");
            btn_ok.setText("D'accord");

        }

        seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                try {
                    if (temp_value == 0)
                        temp_value = 0.8f;
                    updatePreference(temp_value, tv_text.getText().toString());
                    //	Toast.makeText(context,"speed set", Toast.LENGTH_SHORT).show();
                    Log.w("speed_btn", "" + temp_value);

                } catch (Exception e) {
                    // TODO: handle exception
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                // TODO Auto-generated method stub

                float value = (float) progress / 100;
                value = (float) (value * 0.5);
                value = (float) (value + 0.5);


//				String v=""+value;


                //			value=(float) (value*0.3);
                //			value=(float) (value/0.5);
                //		value=(float) (value+0.2);


                DecimalFormat df2 = new DecimalFormat(decimalFormatStr);
                double d = value;
                Log.d("formatted", df2.format(d));
                tv_text.setText(df2.format(d));

                temp_value = value;

                //			Log.w("speed", ""+temp_value);


            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (temp_value == 0)
                    temp_value = 0.8f;
                updatePreference(temp_value, tv_text.getText().toString());
//				Toast.makeText(context,"speed set", Toast.LENGTH_SHORT).show();
                Log.w("speed_btn", "" + temp_value);
                dismiss();
            }
        });

    }


    private void updatePreference(float value, String f) {

        SharedPreferences preference = context.getSharedPreferences("Speed", Context.MODE_PRIVATE);
        Editor editor = preference.edit();
        editor.putFloat("speed", value);
        editor.putString("text", f);
        editor.commit();


    }

    private void getPreference() {

        SharedPreferences preference = context.getSharedPreferences("Speed", Context.MODE_PRIVATE);
        float value = preference.getFloat("speed", 1.0f);
        String t = preference.getString("text", "1.00");
        temp_value = value;
        Log.d("formatted 1", t);
        tv_text.setText(t);


        //	value=(float) (value/0.8);

        Log.w("speed_get", "" + value);

        value = value * 100;

        float last = Float.parseFloat(t);
        Log.d("formatted 2", t);
        tv_text.setText(t);

        last = last - 0.5f;

        last = (float) (last + last);
        last = last * 100;
        seekbar.setProgress(Math.round(last));
        Log.w("speed_round", "" + value);

        //tv_text.setText(""+value);

    }

    @Override
    public void setOnDismissListener(OnDismissListener listener) {
        // TODO Auto-generated method stub
        //super.setOnDismissListener(listener);


    }

}
